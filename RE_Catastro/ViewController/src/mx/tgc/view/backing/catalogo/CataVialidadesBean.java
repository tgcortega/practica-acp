package mx.tgc.view.backing.catalogo;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.validator.ValidatorException;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;
import mx.tgc.utilityfwk.view.search.SearchComponent;
import mx.tgc.utilityfwk.view.search.Searchable;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataVialidadesBean extends GenericBean implements Searchable {

  private RichTable tabla;
  private RichPopup popupDml;
  private PropertiesReader properties;

  public void setTabla(RichTable tabla) {
    this.tabla = tabla;
  }

  public RichTable getTabla() {
    return tabla;
  }

  private void mostrarMensajeOperacion(String keyMsg) {
    FacesContext fc = FacesContext.getCurrentInstance();
    try {
      String msj =
        new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                             CataSegCatasUbicaValBean.class).getMessage(keyMsg);
      fc.addMessage(null, new FacesMessage(msj));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        RequestContext rc = RequestContext.getCurrentInstance();
        BindingContainer bindings = getBindings();
        if (!rc.isPostback()) {
            //Obtiene la recaudaci�n base del usuario para filtrar la tabla
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
            Map scopeVar = facesCtx.getPageFlowScope();
            String s = (String)scopeVar.get("catalogo");
            String munSel = (String)scopeVar.get("municipioSel");

            if (munSel != null && !"".equals(munSel)) {
                idsMunicipios = munSel;
            }

            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }

            OperationBinding ob1 = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
            ob1.getParamsMap().put("muniId", idsMunicipios);
            ob1.execute();

            OperationBinding filtrarActivos = bindings.getOperationBinding("filtrarActivos");
            filtrarActivos.getParamsMap().put("view", "PcVialidadesVO3");
            if (s == null) {
                scopeVar.put("catalogo", "true");
                filtrarActivos.getParamsMap().put("filtrar", false);
            } else {
                scopeVar.put("catalogo", "false");
                filtrarActivos.getParamsMap().put("filtrar", true);
            }
            filtrarActivos.execute();

            String debug = super.getParametroGeneral("AA0008");
            if (debug == null || debug.equals("")) {
                String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }

        }

    }

  /**
   * M�todo que sube al PageFlowScope los valores que regresar� el cat�logo
   * teniendo comportamiento de pop up.
   * @return
   * @autor Ang�lica Ledezma
   * @since 14/09/2011
   */
  public String subirValores() {
    Iterator iter = getTabla().getSelectedRowKeys().iterator();
    if (iter != null && iter.hasNext()) {
      JUCtrlHierNodeBinding rowData =
        (JUCtrlHierNodeBinding)getTabla().getSelectedRowData();
      Row row = rowData.getCurrentRow();
      AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
      Map scopeVar = facesCtx.getPageFlowScope();
      scopeVar.put("idVialidad", row.getAttribute("Identificador"));
      scopeVar.put("idLocaVialidad", row.getAttribute("PcLocaIdentificador"));
      return "exit";
    } else {
      String msj = "Favor de seleccionar una vialidad.";
      mostrarMensaje(FacesMessage.SEVERITY_INFO, msj);
      return null;
    }
  }

  /**
   * Este m�todo sirve para borrar un registro de la tabla
   * vialidades, es necesario ver el manual de creaci�n
   * maestro-detalle para ver la implementaci�n del m�todo.
   *
   * @return
   */
  public String borrarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Delete");
    ob.execute();

    OperationBinding operationBinding2 =
      bindings.getOperationBinding("Commit");
    operationBinding2.execute();

    if (!ob.getErrors().isEmpty() ||
        !operationBinding2.getErrors().isEmpty()) {
      mostrarMensajeOperacion("SYS.VIALIDAD_UTILIZADA");
      OperationBinding obRollback = bindings.getOperationBinding("Rollback");
      obRollback.execute();
      return null;
    }
    mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
    return null;
  }

  /**
   * Este m�todo sirve para insertar un registro de la tabla
   * vialidades, es necesario ver el manual de creaci�n
   * maestro-detalle para ver la implementaci�n del m�todo.
   *
   * @return
   */
  public String insertarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Commit");
    ob.execute();
    FacesContext fc = FacesContext.getCurrentInstance();
    if (!ob.getErrors().isEmpty()) {
      mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
      return null;
    }
    mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
    ocultaPopup();
    return null;
  }

  public RichPanelGroupLayout getPanelBusqueda() {
    RequestContext rc = RequestContext.getCurrentInstance();
    if (!rc.isPostback()) {
      this.setColumnas(1);
      this.setSearchComponents(prepararBusqueda());
    }
    return this.createDynamicPanelForm();
  }

  public List<SearchComponent> prepararBusqueda() {
    List<SearchComponent> l = new ArrayList<SearchComponent>();
    SearchComponent sc1 = new SearchComponent("CataVialidadesBean");
    sc1.setComponente(SearchComponent.INPUT_TEXT);
    sc1.setAtributo("ClaveVialidad");
    sc1.setLabel("Clave de vialidad:");
    sc1.setSize(15);
    sc1.setId("txtClave");

    SearchComponent sc2 = new SearchComponent("CataVialidadesBean");
    sc2.setComponente(SearchComponent.SELECT_ONE_CHOICE);
    sc2.setAtributo("TipoVialidad");
    sc2.setLabel("Tipo de vialidad:");
    sc2.setId("socTipoVialidad");
    sc2.setDominio("PC_VIALIDADES.TIPO_VIALIDAD");


    SearchComponent sc3 = new SearchComponent("CataVialidadesBean");
    sc3.setComponente(SearchComponent.INPUT_TEXT);
    sc3.setAtributo("NombreVialidad");
    sc3.setLabel("Nombre de vialidad:");
    sc3.setSize(18);
    sc3.setId("txtNombreVialidad");

    
    l.add(sc1);
    l.add(sc2);
    l.add(sc3);
    
      AdfFacesContext facesCtx = AdfFacesContext.getCurrentInstance();
      Map scopeVar = facesCtx.getPageFlowScope();
      String s = (String)scopeVar.get("catalogo");
      Boolean catalogo=false;
      if(s!=null){
          catalogo=Boolean.parseBoolean(s);
      }
      //Solo agrega la busqueda de estatus cuando el catalogo
      //sea mandado llamar como catalogo
      if(catalogo){
          SearchComponent sc4 = new SearchComponent("CataVialidadesBean");
          sc4.setComponente(SearchComponent.SELECT_ONE_CHOICE);
          sc4.setAtributo("Estatus");
          sc4.setLabel("Estatus:");
          sc4.setId("socEstatus");
          sc4.setDominio("PC_VIALIDADES.ESTATUS");
          l.add(sc4);
      }
      
    return l;
  }

  public BindingContainer getBindings() {
    return BindingContext.getCurrent().getCurrentBindingsEntry();
  }

  public void cancelAction(DialogEvent dialogEvent) {
    if (dialogEvent.getOutcome() == DialogEvent.Outcome.ok) {
      BindingContainer bindings = getBindings();
      OperationBinding operationBinding2 =
        bindings.getOperationBinding("Rollback");
      operationBinding2.execute();
      //Object result2 = operationBinding2.execute();
      AttributeBinding op1 =
        (AttributeBinding)getBindings().getControlBinding("modifica1");
      op1.setInputValue("false");
    }
  }

  public void muestrapopDml(ActionEvent actionEvent) {
      AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
    DCBindingContainer dcBindings = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
    DCIteratorBinding iterBind= (DCIteratorBinding)dcBindings.get("PcVialidadesVO2Iterator");
    Map pfs = fc.getPageFlowScope();
    String operacion = pfs.get("operacion").toString();
    if (operacion != null && operacion.equals("modificar")) {
          if (iterBind.getCurrentRow() == null){
              mostrarMensaje(FacesMessage.SEVERITY_WARN, "Debe seleccionar un registro para poder modificarlo"); 
              return;
          }
    }
    muestraPopup();
  }

  public String muestraPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                          "\").show();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }

  public void setPopupDml(RichPopup popupDml) {
    this.popupDml = popupDml;
  }

  public RichPopup getPopupDml() {
    return popupDml;
  }

  public void formaPopupFetchListener(PopupFetchEvent popupFetchEvent) {
    AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
    String clientId = popupFetchEvent.getLaunchSourceClientId();
    Map pfs = fc.getPageFlowScope();
    String operacion = pfs.get("operacion").toString();
    if (operacion != null && operacion.equals("nuevo")) {
      OperationBinding operationBinding =
        getBindings().getOperationBinding("CreateInsert");
      operationBinding.execute();
    }else if(operacion != null && operacion.equals("modificar")){
        AttributeBinding claveVialidad=(AttributeBinding)this.getBindings().getControlBinding("ClaveVialidad");
        if(claveVialidad!=null){
           this.getPageFlowScope().put("claveVialidad", claveVialidad.getInputValue());
        }
      
    }
  }

  public String ocultaPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                          "\").hide();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }


    /**
     * Valida que la clave vialidad no se encuentre asignada a un municipio.
     *
     * @param facesContext
     * @param uIComponent
     * @param object
     * @author Juan Carlos Olivas
     * @date 21/04/2014
     */
    public void claveVialidadValidator(FacesContext facesContext,
                                       UIComponent uIComponent,
                                       Object object) {
        Map res;
        if(object!=null){
            BindingContainer bindings = getBindings();
            OperationBinding ob =
            bindings.getOperationBinding("validaClaveVialidad");
            ob.getParamsMap().put("claveVialidad", object.toString());
            res=(Map)ob.execute();
            //si la claveAsentamiento ya existe
            if(res.containsKey("resultado")){
                
                AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
                Map pfs = fc.getPageFlowScope();
                String operacion = pfs.get("operacion").toString();
                //Si es una actualizacion
                if(operacion != null && operacion.equals("modificar")){
                
                   String claveVialidad= this.getPageFlowScope().get("claveVialidad").toString();
                   //se valida que no se utilice una claveAsentamiento ya existente para actualizar
                   if(!claveVialidad.equals(object.toString())){
                       throw new ValidatorException(new FacesMessage(
                               FacesMessage.SEVERITY_ERROR,
                               "La clave de vialidad "+object.toString()+" esta asignada al municipio "+res.get("municipio")+".",
                                                                            null));
                   }
                }else{
                    //Si la clave existe y no es una actualizacion, 
                    //se esta intentando insertar una clave duplicada
                    throw new ValidatorException(new FacesMessage(
                            FacesMessage.SEVERITY_ERROR,
                            "La clave de vialidad "+object.toString()+" esta asignada al municipio "+res.get("municipio")+".",
                                                                         null));
                }    
            }
        }
    }
    
    public void cerrarPopupEscape(PopupCanceledEvent popupCanceledEvent) {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        return;
    }
    
    public void cerrarPopupDml(ActionEvent actionEvent){
        popupDml.cancel();
        resetComponent(popupDml);
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding2 =
          bindings.getOperationBinding("Rollback");
        operationBinding2.execute();
        /*DCBindingContainer bindings1 = (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iter = bindings1.findIteratorBinding("PcVialidadesVO2Iterator");
        iter.executeQuery();*/
        
    }
}
