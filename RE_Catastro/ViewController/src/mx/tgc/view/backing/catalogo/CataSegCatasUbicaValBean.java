package mx.tgc.view.backing.catalogo;

import java.io.IOException;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDialog;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class CataSegCatasUbicaValBean extends GenericBean {
    private RichPopup popupCancelar;
    private RichSelectOneChoice municipios;
    private RichPopup editPopupCancelListener;
    private RichDialog dialog;
    private RichPopup popupDml;
    private PropertiesReader properties;

    /**
     * Se ejecuta el m�todo de persistencia para filtrar los segmentos
     * catastrales por la recaudaci�n del usuario firmado, mandando primero a
     * llamar el m�todo getInfoUser para obtener la recaudaci�n base del usuario
     * y despu�s con el id de la recaudaci�n mandar a llamar el m�todo
     * filtrarPorRecaudacion (CataSegCatastralesAMImpl) y filtra todos los
     * segmentos catastrales de acuerdo a la recaudaci�n del usuario.
     *
     * @param lifecycleContext
     */
    @Override
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        try {
            RequestContext rc = RequestContext.getCurrentInstance();
            if (!rc.isPostback()) {
                String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
                if ("".equals(idsMunicipios)) {
                    muestraPopup("popupRecBase");
                }
                
                BindingContainer bindings = getBindings();
                OperationBinding ob1 = bindings.getOperationBinding("filtrarMunicipiosXUsuario");
                ob1.getParamsMap().put("muniId", idsMunicipios);
                ob1.execute();
                
                try {
                    filtrarSegCataUbica(new oracle.jbo.domain.Number("0"),
                                        new oracle.jbo.domain.Number("0"));
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
                String debug = super.getParametroGeneral("AA0008");
                if (debug == null && debug.equals("")) {
                    String msj =
                        properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
                    mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
                }
            }
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
        //Consulta de par�metro PR0009 que contendr� el valor que llevar� la
        //etiqueta configurable (Colonia o asentamiento).
        String etiqueta = super.getParametroGeneral("PR0009");
        if (etiqueta != null && !etiqueta.equals("")) {
            if (etiqueta.equals("ASENTAMIENTO") ||
                etiqueta.equals("COLONIA")) {
                super.getPageFlowScope().put("PR0009", etiqueta);
            } else {
                super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
                //String msj = properties.getMessage
                mostrarMensajeOperacion("SYS.PARAMETRO_PR0009_NO_EXISTE");
                //mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
            }
        } else {
            super.getPageFlowScope().put("PR0009", "ASENTAMIENTO");
            //String msj = properties.getMessage
            mostrarMensajeOperacion("SYS.PARAMETRO_PR0009_NO_EXISTE");
            //mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
        }
        String debug = super.getParametroGeneral("AA0008");
        if (debug == null && debug.equals("")) {
            String msj =
                properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
        }
    }

    private void mostrarMensajeOperacion(String keyMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            String msj =
                new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                     CataSegCatasUbicaValBean.class).getMessage(keyMsg);
            fc.addMessage(null, new FacesMessage(msj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * M�todo encargado de realizar la creaci�n de un nuevo registro para el
     * llenado de los datos. Esto es al precionar el bot�n Agregar de la tabla
     * de PrValoresUbicacionesVO1.
     * @param popupFetchEvent
     */
    public void editPopupFetchListener(PopupFetchEvent popupFetchEvent) {
        try {
            String clientId = popupFetchEvent.getLaunchSourceClientId();
            if (clientId.contains("Insert")) {
                createInsertOperation();
            }
            FacesContext fc = FacesContext.getCurrentInstance();
            ELContext elctx = fc.getELContext();
            ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
            DCBindingContainer bc =
                (DCBindingContainer)elFactory.createValueExpression(elctx, "#{bindings}", DCBindingContainer.class).getValue(elctx);
            DCIteratorBinding it = bc.findIteratorBinding("AaUsuariosMunicipiosVVO1Iterator");
            for (int i = 0; i < it.getViewObject().getEstimatedRowCount(); i++) {
                Row r = it.getRowAtRangeIndex(i);
                String clave = (String)r.getAttribute("ClaveMunicipio");
                String seleccion = municipios.getValue().toString();
                if (seleccion.equals(clave)) {
                    BindingContainer bindings = getBindings();
                    OperationBinding filtrarAvaluosMunicipio =
                        bindings.getOperationBinding("filtrarAvaluosxMunicipioActivos");
                    filtrarAvaluosMunicipio.getParamsMap().put("municipio",
                                                                 r.getAttribute("Identificador"));
                    filtrarAvaluosMunicipio.execute();
                    return;
                }
            }
            String msj =
                properties.getMessage("SYS.MUNICIPIO_REQ");
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
        } catch (Exception e) {
            String msj =
                properties.getMessage("SYS.MUNICIPIO_REQ");
            mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
        }
    }

    /**
     * M�todo encargado de cancelar el agregar/modificar de un registro al
     * presionar el bot�n Cancelar del popUp del formulario de
     * PrValoresUbicacionesVO1.
     */
    public void editPopupCancelListener(PopupCanceledEvent popupCancelEvent) {
        try {
            rollbackOperation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * M�todo encargado de realizar el agregar/modificar de un registro al
     * presionar el bot�n Aceptar del popUp del formulario de
     * PrValoresUbicacionesVO1.
     * @param dialogEvent
     */
    public void editDialogListener(DialogEvent dialogEvent) {
        try {
            if (dialogEvent.getOutcome().name().equals("ok")) {
                commitOperation();
                mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
            }
        } catch (Exception e) {
            mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
            e.printStackTrace();
        }
    }

    /**
     * M�todo encargado de cancelar el borrado de un registro al presionar el
     * bot�n Cancelar del popUp de confirmaci�n.
     */
    public void deletePopupCancelListener(PopupCanceledEvent popupCancelEvent) {
        try {
            rollbackOperation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * M�todo encargado de realizar el borrado de un registro al presionar el
     * bot�n Aceptar del popUp de confirmaci�n.
     * @param dialogEvent
     */
    public void deleteDialogListener(DialogEvent dialogEvent) {
        if (dialogEvent.getOutcome().name().equals("ok")) {
            BindingContainer bindings = getBindings();
            OperationBinding ob = bindings.getOperationBinding("Delete");
            ob.execute();

            OperationBinding operationBinding2 =
                bindings.getOperationBinding("Commit");
            operationBinding2.execute();

            if (!ob.getErrors().isEmpty() ||
                !operationBinding2.getErrors().isEmpty()) {
                rollbackOperation();
                mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
            } else {
                mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
            }
        }
    }

    /**
     * M�todo que ejecuta el OperationBinding Commit del m�dulo
     * CataSegCatastralesAM.
     */
    private void commitOperation() {
        OperationBinding Commit = getBindings().getOperationBinding("Commit");
        Commit.execute();
    }

    /**
     * M�todo que ejecuta el OperationBinding Rollback del m�dulo
     * CataSegCatastralesAM.
     */
    private void rollbackOperation() {
        try {
            OperationBinding Rollback =
                getBindings().getOperationBinding("Rollback");
            Rollback.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * M�todo que ejecuta el OperationBinding CreateInsert de la vista
     * PrValoresUbicacionesVO1 del m�dulo CataSegCatastralesAM.
     */
    private void createInsertOperation() {
        OperationBinding CreateInsert =
            getBindings().getOperationBinding("CreateInsert");
        CreateInsert.execute();
    }

    /**
     * M�todo encargado de generar los campos de b�squeda para el TreeTable.
     * Se agrega a una lista de SelectItem cada campo que se quiere buscar.
     * @return RichPanelGroupLayout
     */
    @Override
    public RichPanelGroupLayout getPanelBusquedaTreeTable() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            try {
                PropertiesReader propertiesReader =
                    new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                         CataSegCatasUbicaValBean.class);

                this.setTituloPanelBusquedaTreeTable(propertiesReader.getMessage("TIT.SECCION_BUSQUEDA") +
                                                     " " +
                                                     propertiesReader.getMessage("NOM.SEGMENTOS_CATASTRALES"));
                this.setTipPanelBusquedaTreeTable(propertiesReader.getMessage("TIP.BUSQUEDA_TREE_TABLE"));
                this.setBuscarPorTreeTable(propertiesReader.getMessage("LOV.BUSCAR_POR_TREE_TABLE"));
                this.setCriterioBusquedaTreeTable(propertiesReader.getMessage("LOV.CRITERIO_TREE_TABLE"));
                this.setParametroTreeTable(propertiesReader.getMessage("NOM.PARAM_TREE_TABLE"));
                this.setBotonBuscarTreeTable(propertiesReader.getMessage("BOTON.BUSCAR"));
            } catch (IOException ioe) {
                throw new JboException(ioe.getMessage());
            }

            List<SelectItem> campos = new ArrayList<SelectItem>();
            campos.add(new SelectItem("Campo15", "Completa"));
            setManagedBean("CataSegCatasUbicaValBean");
            setCamposDeBusquedaTreeTable(campos);
        }
        return super.getPanelBusquedaTreeTable();
    }

    public void onRowSelection(SelectionEvent selectionEvent) {
        this.getBindings();
        invokeMethodExpression("#{bindings.PrSegmentosCatastralesVO1.treeModel.makeCurrent}",
                               Object.class, SelectionEvent.class,
                               selectionEvent);
        RichTreeTable tree = getTreeTable();
        RowKeySet rks = tree.getSelectedRowKeys();
        Iterator rksIterator = rks.iterator();
        CollectionModel model = (CollectionModel)tree.getValue();
        JUCtrlHierBinding treeBinding =
            (JUCtrlHierBinding)model.getWrappedData();
        Row rw = null;
        while (rksIterator.hasNext()) {
            List nodeKey = (List)rksIterator.next();
            JUCtrlHierNodeBinding node =
                treeBinding.findNodeByKeyPath(nodeKey);
            rw = node.getRow();
        }
        try {
            filtrarSegCataUbica(new oracle.jbo.domain.Number(rw.getAttribute("Serie").toString()),
                                new oracle.jbo.domain.Number(rw.getAttribute("Identificador").toString()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Invoke an expression
     * @param expr
     * @param returnType
     * @param argType
     * @param argument
     * @return
     */
    public static Object invokeMethodExpression(String expr, Class returnType,
                                                Class argType,
                                                Object argument) {
        return invokeMethodExpression(expr, returnType,
                                      new Class[] { argType },
                                      new Object[] { argument });
    }

    /**
     * Invokes an expression
     * @param expr
     * @param returnType
     * @param argTypes
     * @param args
     * @return
     */
    public static Object invokeMethodExpression(String expr, Class returnType,
                                                Class[] argTypes,
                                                Object[] args) {
        FacesContext fc = FacesContext.getCurrentInstance();
        ELContext elctx = fc.getELContext();
        ExpressionFactory elFactory =
            fc.getApplication().getExpressionFactory();
        MethodExpression methodExpr =
            elFactory.createMethodExpression(elctx, expr, returnType,
                                             argTypes);
        return methodExpr.invoke(elctx, args);
    }

    /**
     * M�todo que manda llamar el m�todo filtrarBloquesPredios en persistencia
     * para mostrar en la forma de captura el bloque que se desea modificar.
     *
     * @param    cataSerie
     * @param    cataId
     * @author   enunez
     * @date     09/09/2011
     */
    private void filtrarSegCataUbica(oracle.jbo.domain.Number cataSerie,
                                     oracle.jbo.domain.Number cataId) {
        OperationBinding op =
            (OperationBinding)this.getBindings().getControlBinding("filtraSegCataUbica");
        op.getParamsMap().put("cataSerie", cataSerie);
        op.getParamsMap().put("cataId", cataId);
        op.execute();
    }

    public void setPopupCancelar(RichPopup popupCancelar) {
        this.popupCancelar = popupCancelar;
    }

    public RichPopup getPopupCancelar() {
        return popupCancelar;
    }

    public void setEditPopupCancelListener(RichPopup editPopupCancelListener) {
        this.editPopupCancelListener = editPopupCancelListener;
    }

    public RichPopup getEditPopupCancelListener() {
        return editPopupCancelListener;
    }

    public void setDialog(RichDialog dialog) {
        this.dialog = dialog;
    }

    public RichDialog getDialog() {
        return dialog;
    }

    public void cancelarListener(DialogEvent dialogEvent) {
        try {
            rollbackOperation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String ocultaPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").hide();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n", null));
        }
        return null;
    }

    public void aceptarListener(ActionEvent actionEvent) {
        commitOperation();
        refreshTable();
        ocultaPopup();
    }

    public void setPopupDml(RichPopup popupDml) {
        this.popupDml = popupDml;
    }

    public RichPopup getPopupDml() {
        return popupDml;
    }

    public String muestraPopup() {
        try {
            ExtendedRenderKitService erks =
                Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                            ExtendedRenderKitService.class);
            StringBuilder strb =
                new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                  this.popupDml.getClientId(FacesContext.getCurrentInstance()) +
                                  "\").show();");
            erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
        } catch (Exception e) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null,
                               new FacesMessage(FacesMessage.SEVERITY_INFO,
                                                "Error en operaci�n", null));
        }
        return null;
    }

    public void muestraPopListener(ActionEvent actionEvent) {
        muestraPopup();
    }

    public void cancelarPopListener(ActionEvent actionEvent) {
        try {
            /**
              * Fecha:08/05/2015
              * Origen:RE - Catastro - MR0017_1 - DGO - Caracter�sticas valuaci�n - Al cancelar un alta y volver a entrar a esa secci�n. El sistema muestra los valores de alta cancelada
              * Autor:Brenda Gomez.
              * */
            popupDml.cancel();
            resetComponent(popupDml);
            rollbackOperation();           
        } catch (Exception e) {
            e.printStackTrace();
        }
        ocultaPopup();
    }

    public void setMunicipios(RichSelectOneChoice municipios) {
        this.municipios = municipios;
    }

    public RichSelectOneChoice getMunicipios() {
        return municipios;
    }
}
