package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;
import mx.tgc.utilityfwk.view.search.SearchComponent;
import mx.tgc.utilityfwk.view.search.Searchable;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataValoresUnitValuacionPredialBean extends GenericBean implements Searchable {
    private RichTable tablaValoresUnitarios;
    private RichTable tablaCaracteristica;
    private boolean agregar = false;
    private RichInputText caracteristica;
  private RichPopup formaPopup;

  public CataValoresUnitValuacionPredialBean() {
    }

    public void setTablaValoresUnitarios(RichTable tablaValoresUnitarios) {
        this.tablaValoresUnitarios = tablaValoresUnitarios;
    }

    public RichTable getTablaValoresUnitarios() {
        return tablaValoresUnitarios;
    }

    public void setTablaCaracteristica(RichTable tablaCaracteristica) {
        this.tablaCaracteristica = tablaCaracteristica;
    }

    public RichTable getTablaCaracteristica() {
        return tablaCaracteristica;
    }

    public void setCaracteristica(RichInputText caracteristica) {
        this.caracteristica = caracteristica;
    }

    public RichInputText getCaracteristica() {
        return caracteristica;
    }

    /**
     * @Modified: 04/06/2015
     * @By: E. Alejandro Becerril.
     * @param lifecycleContext
     * @Desc: se cambio el metodo de filtrado de recaudacion a municipio
     */
    public void prepareModel(LifecycleContext lifecycleContext) {
        super.prepareModel(lifecycleContext);
        if (!RequestContext.getCurrentInstance().isPostback()) {
            String idsMunicipios = getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
            if ("".equals(idsMunicipios)) {
                muestraPopup("popupRecBase");
            }
            BindingContainer bindings = getBindings();
            OperationBinding filtrarPorRecaudacion =
            bindings.getOperationBinding("filtrarMunicipiosActivos");
            filtrarPorRecaudacion.getParamsMap().put("muniId", idsMunicipios);
            filtrarPorRecaudacion.execute();
        }
    }

    private void mostrarMensajeOperacion(String keyMsg) {
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
            String msj =
                new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                     CataValoresUnitValuacionPredialBean.class).getMessage(keyMsg);
            fc.addMessage(null, new FacesMessage(msj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Este m�todo sirve para insertar un registro de la tabla
     * PC_VALORES_UNITARIOS, es necesario ver el manual
     * de creaci�n maestro-detalle para ver la implementaci�n del m�todo.
     *
     * @return
     */
    public String insertarRegistro() {
        BindingContainer bindings = getBindings();
        OperationBinding ob = bindings.getOperationBinding("Commit");
        ob.execute();
        if (!ob.getErrors().isEmpty()) {
            mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
            return null;
        }
        mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
        return null;
    }

    /**
     * Este m�todo sirve para borrar un registro de la tabla
     * PC_VALORES_UNITARIOSS, es necesario ver el manual
     * de creaci�n maestro-detalle para ver la implementaci�n del m�todo.
     *
     * @return
     */
    public String borrarRegistro() {
        BindingContainer bindings = getBindings();
        OperationBinding ob = bindings.getOperationBinding("Delete");
        ob.execute();
        getBindings();
        OperationBinding operationBinding2 =
            bindings.getOperationBinding("Commit");
        operationBinding2.execute();
        System.out.println(ob.getErrors());
        ob.getErrors();
        if (!ob.getErrors().isEmpty() ||
            !operationBinding2.getErrors().isEmpty()) {
            OperationBinding operationBinding3 =
                bindings.getOperationBinding("Rollback");
            operationBinding3.execute();
            mostrarMensajeOperacion("SYS.OPERACION_FALLIDA");
            return null;
        }
        mostrarMensajeOperacion("SYS.OPERACION_EXITOSA");
        return null;
    }

    public List<SearchComponent> prepararBusqueda() {
        List<SearchComponent> l = new ArrayList<SearchComponent>();
        SearchComponent sc1 = new SearchComponent("CataValoresUnitValuacion");
        sc1.setComponente(SearchComponent.INPUT_TEXT);
        sc1.setAtributo("Nombre");
        sc1.setSize(20);
        sc1.setLabel("Nombre:");
        sc1.setId("txtNombre");

        SearchComponent sc3 = new SearchComponent("CataValoresUnitValuacion");
        sc3.setComponente(SearchComponent.INPUT_TEXT);
        sc3.setAtributo("Etiqueta");
        sc3.setSize(20);
        sc3.setLabel("Etiqueta:");
        sc3.setId("txtEtiqueta");

        SearchComponent sc2 = new SearchComponent("CataValoresUnitValuacion");
        sc2.setComponente(SearchComponent.SELECT_ONE_CHOICE);
        sc2.setAtributo("Nivel");
        sc2.setLabel("Nivel:");
        sc2.setId("socTipoNivel");
        sc2.setDominio("PR_PREDIAL_CARACTERISTICAS.NIVEL");

        SearchComponent sc4 = new SearchComponent("CataValoresUnitValuacion");
        sc4.setComponente(SearchComponent.SELECT_ONE_CHOICE);
        sc4.setAtributo("Comportamiento");
        sc4.setLabel("Comportamiento:");
        sc4.setId("socComporamiento");
        sc4.setDominio("PR_PREDIAL_CARACTERISTICAS.COMPORTAMIENTO");
        l.add(sc1);
        l.add(sc3);
        l.add(sc2);
        l.add(sc4);
        return l;
    }

    public RichPanelGroupLayout getPanelBusqueda() {
        RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            this.setColumnas(1);
            this.setSearchComponents(prepararBusqueda());
        }
        return this.createDynamicPanelForm();
    }

    public void formaPopUpFetchListener(PopupFetchEvent popupFetchEvent) {
        String clientId = popupFetchEvent.getLaunchSourceClientId();
        AttributeBinding caracter =
            (AttributeBinding)this.getBindings().getControlBinding("Nombre");
        this.getCaracteristica().setValue(caracter.getInputValue().toString());
        if (clientId.contains("Insert")) {
            agregar = true;
            OperationBinding operationBinding =
                getBindings().getOperationBinding("Create");
            operationBinding.execute();

        } else if (popupFetchEvent.getLaunchSourceClientId().contains("Edit")) {
            agregar = false;
            oracle.adf.model.AttributeBinding id1 =
                (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("Identificador");
            oracle.adf.model.AttributeBinding ser1 =
                (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("Serie");

            oracle.adf.model.AttributeBinding id2 =
                (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("PrPrcaIdentificador");
            oracle.adf.model.AttributeBinding ser2 =
                (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("PrPrcaSerie");

            id2.setInputValue(id1);
            ser2.setInputValue(ser1);

        }
    }

    public void formaPopUpCancelarListener(PopupCanceledEvent popupCanceledEvent) {
        if (agregar) {
            rollbackOperation();
            AdfFacesContext adfFacesContext =
                AdfFacesContext.getCurrentInstance();
            adfFacesContext.addPartialTarget(tablaCaracteristica);
            adfFacesContext.addPartialTarget(tablaValoresUnitarios);
        }
    }

    private void rollbackOperation() {
        OperationBinding operationBinding =
            getBindings().getOperationBinding("Rollback");
        operationBinding.execute();
    }

  public void cancelarListener(ActionEvent actionEvent) {
    rollbackOperation();
    AdfFacesContext adfFacesContext =
        AdfFacesContext.getCurrentInstance();
    adfFacesContext.addPartialTarget(tablaCaracteristica);
    adfFacesContext.addPartialTarget(tablaValoresUnitarios);
    ocultaPopup();
    this.resetComponent(formaPopup);
  }
  
  public String ocultaPopup() {
      try {
          ExtendedRenderKitService erks =
              Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                          ExtendedRenderKitService.class);
          StringBuilder strb =
              new StringBuilder("AdfPage.PAGE.findComponent(\"" +
                                this.formaPopup.getClientId(FacesContext.getCurrentInstance()) +
                                "\").hide();");
          erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
      } catch (Exception e) {
          FacesContext context = FacesContext.getCurrentInstance();
          context.addMessage(null,
                             new FacesMessage(FacesMessage.SEVERITY_INFO,
                                              "Error en operaci�n", null));
      }
      return null;
  }

  public void setFormaPopup(RichPopup formaPopup) {
    this.formaPopup = formaPopup;
  }

  public RichPopup getFormaPopup() {
    return formaPopup;
  }

  public void aceptarListener(ActionEvent actionEvent) {
    insertarRegistro();
    AdfFacesContext adfFacesContext =
        AdfFacesContext.getCurrentInstance();
    adfFacesContext.addPartialTarget(tablaValoresUnitarios);
    ocultaPopup();
  }
}
