package mx.tgc.view.backing.consulta;

import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import mx.net.tgc.recaudador.resource.pc.DomicilioNotificar;
import mx.net.tgc.recaudador.resource.pr.Predio;

import mx.tgc.model.util.JavaBeanKeys;
import mx.tgc.utilityfwk.resource.LovBean;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichQuery;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adf.view.rich.event.QueryEvent;
import oracle.adf.view.rich.event.QueryOperationEvent;
import oracle.adf.view.rich.model.QueryDescriptor;
import oracle.adf.view.rich.model.QueryModel;

import oracle.binding.BindingContainer;

import oracle.jbo.JboException;
import oracle.jbo.ViewObject;
import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.event.DisclosureEvent;


public class ConsultaBusquedaBean extends GenericBean {
    private RichTable tablaConsulta;
    private RichQuery claveActualQuery;
    private RichQuery claveAnteriorQuery;
    private RichQuery fisicaNombreQuery;
    private RichQuery fisicaRFCQuery;
    private RichQuery fisicaCURPQuery;
    private RichQuery moralRazonSocialQuery;
    private RichQuery moralRFCQuery;
    private RichQuery moralNombreComercialQuery;
    private RichQuery depuradoQuery;
    private RichQuery noDepuradoQuery;
    private RichQuery cruzadaQuery;
    private RichShowDetailItem actualTab;
    private RichShowDetailItem anteriorTab;
    private RichShowDetailItem fisicaNombreTab;
    private RichShowDetailItem fisicaRFCTab;
    private RichShowDetailItem fisicaCURPTab;
    private RichShowDetailItem moralRazonSocialTab;
    private RichShowDetailItem moralRFCTab;
    private RichShowDetailItem moralNombreComercialTab;
    private RichShowDetailItem depuradoTab;
    private RichShowDetailItem noDepuradoTab;
    private RichShowDetailItem cruzadaTab;
    private RichShowDetailItem claveCatastralTab;
    private RichShowDetailItem propietarioTab;
    private RichShowDetailItem domicilioTab;
    private RichShowDetailItem fisicaTab;
    private RichShowDetailItem moralTab;

    public ConsultaBusquedaBean() {
    }

    public void setTablaConsulta(RichTable tablaConsulta) {
        this.tablaConsulta = tablaConsulta;
    }

    public RichTable getTablaConsulta() {
        return tablaConsulta;
    }

    public void setClaveActualQuery(RichQuery claveActualQuery) {
        this.claveActualQuery = claveActualQuery;
    }

    public RichQuery getClaveActualQuery() {
        return claveActualQuery;
    }

    public void setClaveAnteriorQuery(RichQuery claveAnteriorQuery) {
        this.claveAnteriorQuery = claveAnteriorQuery;
    }

    public RichQuery getClaveAnteriorQuery() {
        return claveAnteriorQuery;
    }

    public void setFisicaNombreQuery(RichQuery fisicaNombreQuery) {
        this.fisicaNombreQuery = fisicaNombreQuery;
    }

    public RichQuery getFisicaNombreQuery() {
        return fisicaNombreQuery;
    }

    public void setFisicaRFCQuery(RichQuery fisicaRFCQuery) {
        this.fisicaRFCQuery = fisicaRFCQuery;
    }

    public RichQuery getFisicaRFCQuery() {
        return fisicaRFCQuery;
    }

    public void setFisicaCURPQuery(RichQuery fisicaCURPQuery) {
        this.fisicaCURPQuery = fisicaCURPQuery;
    }

    public RichQuery getFisicaCURPQuery() {
        return fisicaCURPQuery;
    }

    public void setMoralRazonSocialQuery(RichQuery moralRazonSocialQuery) {
        this.moralRazonSocialQuery = moralRazonSocialQuery;
    }

    public RichQuery getMoralRazonSocialQuery() {
        return moralRazonSocialQuery;
    }

    public void setMoralRFCQuery(RichQuery moralRFCQuery) {
        this.moralRFCQuery = moralRFCQuery;
    }

    public RichQuery getMoralRFCQuery() {
        return moralRFCQuery;
    }

    public void setMoralNombreComercialQuery(RichQuery moralNombreComercialQuery) {
        this.moralNombreComercialQuery = moralNombreComercialQuery;
    }

    public RichQuery getMoralNombreComercialQuery() {
        return moralNombreComercialQuery;
    }

    public void setDepuradoQuery(RichQuery depuradoQuery) {
        this.depuradoQuery = depuradoQuery;
    }

    public RichQuery getDepuradoQuery() {
        return depuradoQuery;
    }

    public void setNoDepuradoQuery(RichQuery noDepuradoQuery) {
        this.noDepuradoQuery = noDepuradoQuery;
    }

    public RichQuery getNoDepuradoQuery() {
        return noDepuradoQuery;
    }

    public void setCruzadaQuery(RichQuery cruzadaQuery) {
        this.cruzadaQuery = cruzadaQuery;
    }

    public RichQuery getCruzadaQuery() {
        return cruzadaQuery;
    }

    public void setActualTab(RichShowDetailItem actualTab) {
        this.actualTab = actualTab;
    }

    public RichShowDetailItem getActualTab() {
        return actualTab;
    }

    public void setAnteriorTab(RichShowDetailItem anteriorTab) {
        this.anteriorTab = anteriorTab;
    }

    public RichShowDetailItem getAnteriorTab() {
        return anteriorTab;
    }

    public void setFisicaNombreTab(RichShowDetailItem fisicaNombreTab) {
        this.fisicaNombreTab = fisicaNombreTab;
    }

    public RichShowDetailItem getFisicaNombreTab() {
        return fisicaNombreTab;
    }

    public void setFisicaRFCTab(RichShowDetailItem fisicaRFCTab) {
        this.fisicaRFCTab = fisicaRFCTab;
    }

    public RichShowDetailItem getFisicaRFCTab() {
        return fisicaRFCTab;
    }

    public void setFisicaCURPTab(RichShowDetailItem fisicaCURPTab) {
        this.fisicaCURPTab = fisicaCURPTab;
    }

    public RichShowDetailItem getFisicaCURPTab() {
        return fisicaCURPTab;
    }

    public void setMoralRazonSocialTab(RichShowDetailItem moralRazonSocialTab) {
        this.moralRazonSocialTab = moralRazonSocialTab;
    }

    public RichShowDetailItem getMoralRazonSocialTab() {
        return moralRazonSocialTab;
    }

    public void setMoralRFCTab(RichShowDetailItem moralRFCTab) {
        this.moralRFCTab = moralRFCTab;
    }

    public RichShowDetailItem getMoralRFCTab() {
        return moralRFCTab;
    }

    public void setMoralNombreComercialTab(RichShowDetailItem moralNombreComercialTab) {
        this.moralNombreComercialTab = moralNombreComercialTab;
    }

    public RichShowDetailItem getMoralNombreComercialTab() {
        return moralNombreComercialTab;
    }

    public void setDepuradoTab(RichShowDetailItem depuradoTab) {
        this.depuradoTab = depuradoTab;
    }

    public RichShowDetailItem getDepuradoTab() {
        return depuradoTab;
    }

    public void setNoDepuradoTab(RichShowDetailItem noDepuradoTab) {
        this.noDepuradoTab = noDepuradoTab;
    }

    public RichShowDetailItem getNoDepuradoTab() {
        return noDepuradoTab;
    }

    public void setCruzadaTab(RichShowDetailItem cruzadaTab) {
        this.cruzadaTab = cruzadaTab;
    }

    public RichShowDetailItem getCruzadaTab() {
        return cruzadaTab;
    }
    

    public void setClaveCatastralTab(RichShowDetailItem claveCatastralTab) {
        this.claveCatastralTab = claveCatastralTab;
    }
  
    public RichShowDetailItem getClaveCatastralTab() {
        return claveCatastralTab;
    }
  
    public void setPropietarioTab(RichShowDetailItem propietarioTab) {
        this.propietarioTab = propietarioTab;
    }
  
    public RichShowDetailItem getPropietarioTab() {
        return propietarioTab;
    }
  
    public void setDomicilioTab(RichShowDetailItem domicilioTab) {
        this.domicilioTab = domicilioTab;
    }
  
    public RichShowDetailItem getDomicilioTab() {
        return domicilioTab;
    }
  
    public void setFisicaTab(RichShowDetailItem fisicaTab) {
        this.fisicaTab = fisicaTab;
    }
  
    public RichShowDetailItem getFisicaTab() {
        return fisicaTab;
    }
  
    public void setMoralTab(RichShowDetailItem moralTab) {
        this.moralTab = moralTab;
    }
  
    public RichShowDetailItem getMoralTab() {
        return moralTab;
    }

    private Map getProcessBean() {
        AdfFacesContext processBean;
        processBean = AdfFacesContext.getCurrentInstance();
        Map beanSessionMap = processBean.getPageFlowScope();
        return beanSessionMap;
    }

    public String actionSiguiente() {
        try {
            BindingContainer bindings = this.getBindings();
            OperationBinding datosPredio =
                (OperationBinding)bindings.getOperationBinding("getDatosPredio");

            Map pFScope = this.getProcessBean();
            pFScope.remove(JavaBeanKeys.PR_PREDIO);

            Number predSerie = (Number)pFScope.get("serie");
            Number predId = (Number)pFScope.get("identificador");

            datosPredio.getParamsMap().put("predSerie", predSerie.intValue());
            datosPredio.getParamsMap().put("predId", predId.longValue());
            Predio predio = (Predio)datosPredio.execute();

            if (datosPredio.getErrors().isEmpty()) {
                pFScope.put(JavaBeanKeys.PR_PREDIO, predio);
                DomicilioNotificar dom = null;
                LovBean lv = new LovBean();
                String domicilioRef = "";
                if (predio.getPropietario().getPropietario() != null) {
                    dom =
predio.getPropietario().getPropietario().getDomicilio();
                    domicilioRef =
                            lv.getDescripcionCorta("PC_DOMICILIOS_A_NOTIFICAR.TIPO_CALLE", dom.getTipoCalle())
                            + " " + dom.getCalle() + " " +
                            dom.getNumeroExterior() + " " + 
                            ((dom.getLetra()!=null)?dom.getLetra():"") +
                            " " + 
                            ((dom.getNumeroInterior()!=null)?dom.getNumeroInterior():"")
                            + " " +
                            dom.getColonia() + " " + 
                            ((dom.getCodigoPostal()!=null)?dom.getCodigoPostal():"")
                            +" " + dom.getLocalidad() + " " +
                            ((dom.getMunicipio()!=null)?dom.getMunicipio():"")
                            + " " + 
                            ((dom.getEstado()!=null)?dom.getEstado():"");
                    pFScope.put("domicilioRef", domicilioRef);
                } 
                return "detalles";
            } else {
                String msj = datosPredio.getErrors().get(0).toString();
                FacesContext.getCurrentInstance().addMessage(null,
                                                             new FacesMessage(msj));
            }
        } catch (Exception e) {
            throw new JboException(e.getMessage());
        }
        return null;
    }

    public void restablecerQueriesDisclosureListener(DisclosureEvent disclosureEvent) {
        //Una cl�usula if para cada pesta�a. En cada una se aplica reset al query activo
        OperationBinding ob = (OperationBinding)getBindings().getOperationBinding("resetViewConsulta");
        if(getClaveCatastralTab().isDisclosed()){
            if (getActualTab().isDisclosed()) {
                ob.getParamsMap().put("pesta�a", "ConsultaClaveCatastral");
                ob.execute();
                RichQuery actual = getClaveActualQuery();
                QueryModel queryModelActual = actual.getModel();
                QueryDescriptor queryDescriptorActual = actual.getValue();
                queryModelActual.reset(queryDescriptorActual);
                actual.refresh(FacesContext.getCurrentInstance());
            } else if (getAnteriorTab().isDisclosed()) {
                ob.getParamsMap().put("pesta�a", "ConsultaClaveCatastralAnterior");
                ob.execute();
                RichQuery anterior = getClaveAnteriorQuery();
                QueryModel queryModelAnterior = anterior.getModel();
                QueryDescriptor queryDescriptorAnterior = anterior.getValue();
                queryModelAnterior.reset(queryDescriptorAnterior);
                anterior.refresh(FacesContext.getCurrentInstance());
            }
        } else if(getPropietarioTab().isDisclosed()){
            if(getFisicaTab().isDisclosed()){
                if (getFisicaNombreTab().isDisclosed()) {
                    ob.getParamsMap().put("pesta�a", "ConsultaPersonaFisicaNombre");
                    ob.execute();
                    RichQuery fisicaNombre = getFisicaNombreQuery();
                    QueryModel queryModelFisicaNombre = fisicaNombre.getModel();
                    QueryDescriptor queryDescriptorFisicaNombre = fisicaNombre.getValue();
                    queryModelFisicaNombre.reset(queryDescriptorFisicaNombre);
                    fisicaNombre.refresh(FacesContext.getCurrentInstance());
                } else if (getFisicaCURPTab().isDisclosed()) {
                    ob.getParamsMap().put("pesta�a", "ConsultaPersonaFisicaCURP");
                    ob.execute();
                    RichQuery fisicaCURP = getFisicaCURPQuery();
                    QueryModel queryModelFisicaCURP = fisicaCURP.getModel();
                    QueryDescriptor queryDescriptorFisicaCURP = fisicaCURP.getValue();
                    queryModelFisicaCURP.reset(queryDescriptorFisicaCURP);
                    fisicaCURP.refresh(FacesContext.getCurrentInstance());
                } else if (getFisicaRFCTab().isDisclosed()) {
                    ob.getParamsMap().put("pesta�a", "ConsultaPersonaFisicaRFC");
                    ob.execute();
                    RichQuery fisicaRFC = getFisicaRFCQuery();
                    QueryModel queryModelFisicaRFC = fisicaRFC.getModel();
                    QueryDescriptor queryDescriptorFisicaRFC = fisicaRFC.getValue();
                    queryModelFisicaRFC.reset(queryDescriptorFisicaRFC);
                    fisicaRFC.refresh(FacesContext.getCurrentInstance());
                }
            } else if(getMoralTab().isDisclosed()){
                if (getMoralRazonSocialTab().isDisclosed()) {
                    ob.getParamsMap().put("pesta�a", "ConsultaPersonaMoralRazonSocial");
                    ob.execute();
                    RichQuery moralRazonSocial = getMoralRazonSocialQuery();
                    QueryModel queryModelMoralRazonSocial = moralRazonSocial.getModel();
                    QueryDescriptor queryDescriptorMoralRazonSocial = moralRazonSocial.getValue();
                    queryModelMoralRazonSocial.reset(queryDescriptorMoralRazonSocial);
                    moralRazonSocial.refresh(FacesContext.getCurrentInstance());
                } else if (getMoralRFCTab().isDisclosed()) {
                    ob.getParamsMap().put("pesta�a", "ConsultaPersonaMoralRFC");
                    ob.execute();
                    RichQuery moralRFC = getMoralRFCQuery();
                    QueryModel queryModelMoralRFC = moralRFC.getModel();
                    QueryDescriptor queryDescriptorMoralRFC = moralRFC.getValue();
                    queryModelMoralRFC.reset(queryDescriptorMoralRFC);
                    moralRFC.refresh(FacesContext.getCurrentInstance());
                } else if (getMoralNombreComercialTab().isDisclosed()) {
                    ob.getParamsMap().put("pesta�a", "ConsultaPersonaMoralNombreComercial");
                    ob.execute();
                    RichQuery moralNombreComercial = getMoralNombreComercialQuery();
                    QueryModel queryModelMoralNombreComercial = moralNombreComercial.getModel();
                    QueryDescriptor queryDescriptorMoralNombreComercial = moralNombreComercial.getValue();
                    queryModelMoralNombreComercial.reset(queryDescriptorMoralNombreComercial);
                    moralNombreComercial.refresh(FacesContext.getCurrentInstance());
                }
            }
        } else if(getDomicilioTab().isDisclosed()){
            if (getDepuradoTab().isDisclosed()) {
                ob.getParamsMap().put("pesta�a", "ConsultaDomicilioDepurado");
                ob.execute();
                RichQuery depurado = getDepuradoQuery();
                QueryModel queryModelDepurado = depurado.getModel();
                QueryDescriptor queryDescriptorDepurado = depurado.getValue();
                queryModelDepurado.reset(queryDescriptorDepurado);
                depurado.refresh(FacesContext.getCurrentInstance());
            }else if (getNoDepuradoTab().isDisclosed()) {
                ob.getParamsMap().put("pesta�a", "ConsultaDomicilioNoDepurado");
                ob.execute();
                RichQuery noDepurado = getNoDepuradoQuery();
                QueryModel queryModelNoDepurado = noDepurado.getModel();
                QueryDescriptor queryDescriptorNoDepurado = noDepurado.getValue();
                queryModelNoDepurado.reset(queryDescriptorNoDepurado);
                noDepurado.refresh(FacesContext.getCurrentInstance());
            }
        } else if (getCruzadaTab().isDisclosed()) {
            RichQuery cruzada = getCruzadaQuery();
            QueryModel queryModelCruzada = cruzada.getModel();
            QueryDescriptor queryDescriptorCruzada = cruzada.getValue();
            queryModelCruzada.reset(queryDescriptorCruzada);
            cruzada.refresh(FacesContext.getCurrentInstance());
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(tablaConsulta);     
    }
}
