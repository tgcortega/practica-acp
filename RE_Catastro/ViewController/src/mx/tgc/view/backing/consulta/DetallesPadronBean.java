package mx.tgc.view.backing.consulta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;

import javax.faces.convert.DateTimeConverter;
import javax.faces.event.ActionEvent;

import mx.net.tgc.recaudador.resource.pc.DomicilioNotificar;
import mx.net.tgc.recaudador.resource.pr.PredialCaracteristica;
import mx.net.tgc.recaudador.resource.pr.Predio;
import mx.net.tgc.recaudador.resource.pr.PredioBloqueAgrupacion;


import mx.tgc.model.util.JavaBeanKeys;
import mx.tgc.utilityfwk.resource.LovBean;
import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.view.converters.ConverterKeys;
import mx.tgc.utilityfwk.view.search.UpperCaseConverter;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import mx.tgc.utilityfwk.view.validators.FechaMayorActual;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelFormLayout;
import oracle.adf.view.rich.component.rich.layout.RichPanelHeader;
import oracle.adf.view.rich.component.rich.layout.RichPanelLabelAndMessage;
import oracle.adf.view.rich.component.rich.layout.RichShowDetailItem;
import oracle.adf.view.rich.component.rich.output.RichSpacer;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;

import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.event.DisclosureEvent;


import mx.tgc.utilityfwk.view.validators.ValidatorKeys;

import org.apache.myfaces.trinidad.component.UIXEditableValue;

public class DetallesPadronBean extends GenericBean {
    private RichShowDetailItem showDetailItemDatosPredios;
    private RichShowDetailItem showDetailItemCaractTerreno;
    private RichShowDetailItem showDetailItemCaractConst;
    private RichShowDetailItem showDetailItemHistMov;
    private RichShowDetailItem showDetailItemObligaciones;
    private RichShowDetailItem showDetailItemOtrosPredios;
    private RichPanelFormLayout caracteristicasPanel;
    private RichPanelFormLayout caracteristicasAsentamientosPanel;
    private int indice = 0;
    private List caracteristicasPredios = new ArrayList();
    private RichTable tblPredCompTerreno;
    private RichTable tblPredCompConstruccion;
    private RichPanelFormLayout pnlCaractCompTerreno;
    private RichPanelFormLayout pnlCaractCompConstruccion;
    private RichTable tblOtrosPredios;
    PropertiesReader properties;

    public DetallesPadronBean() {
        super();
        if (properties == null) {
            try {
                properties =
                        new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                             DetallesPadronBean.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    * Getters y Setters
    */

    public void setShowDetailItemDatosPredios(RichShowDetailItem showDetailItemDatosPredios) {
        this.showDetailItemDatosPredios = showDetailItemDatosPredios;
    }

    public RichShowDetailItem getShowDetailItemDatosPredios() {
        return showDetailItemDatosPredios;
    }

    public void setShowDetailItemCaractTerreno(RichShowDetailItem showDetailItemCaractTerreno) {
        this.showDetailItemCaractTerreno = showDetailItemCaractTerreno;
    }

    public RichShowDetailItem getShowDetailItemCaractTerreno() {
        return showDetailItemCaractTerreno;
    }

    public void setShowDetailItemCaractConst(RichShowDetailItem showDetailItemCaractConst) {
        this.showDetailItemCaractConst = showDetailItemCaractConst;
    }

    public RichShowDetailItem getShowDetailItemCaractConst() {
        return showDetailItemCaractConst;
    }

    public void setShowDetailItemHistMov(RichShowDetailItem showDetailItemHistMov) {
        this.showDetailItemHistMov = showDetailItemHistMov;
    }

    public RichShowDetailItem getShowDetailItemHistMov() {
        return showDetailItemHistMov;
    }

    public void setShowDetailItemObligaciones(RichShowDetailItem showDetailItemObligaciones) {
        this.showDetailItemObligaciones = showDetailItemObligaciones;
    }

    public RichShowDetailItem getShowDetailItemObligaciones() {
        return showDetailItemObligaciones;
    }

    public void setShowDetailItemOtrosPredios(RichShowDetailItem showDetailItemOtrosPredios) {
        this.showDetailItemOtrosPredios = showDetailItemOtrosPredios;
    }

    public RichShowDetailItem getShowDetailItemOtrosPredios() {
        return showDetailItemOtrosPredios;
    }

    public void setCaracteristicasPanel(RichPanelFormLayout caracteristicasPanel) {
        this.caracteristicasPanel = caracteristicasPanel;
    }

    public void setCaracteristicasAsentamientosPanel(RichPanelFormLayout caracteristicasAsentamientosPanel) {
        this.caracteristicasAsentamientosPanel =
                caracteristicasAsentamientosPanel;
    }

    public void setTblPredCompTerreno(RichTable tblPredCompTerreno) {
        this.tblPredCompTerreno = tblPredCompTerreno;
    }

    public RichTable getTblPredCompTerreno() {
        return tblPredCompTerreno;
    }

    public void setTblPredCompConstruccion(RichTable tblPredCompConstruccion) {
        this.tblPredCompConstruccion = tblPredCompConstruccion;
    }

    public RichTable getTblPredCompConstruccion() {
        return tblPredCompConstruccion;
    }

    public void setCaracteristicasPredios(List caracteristicasPredios) {
        this.caracteristicasPredios = caracteristicasPredios;
    }

    public List getCaracteristicasPredios() {
        return caracteristicasPredios;
    }

    public RichPanelFormLayout getCaracteristicasPanel() {
        return this.createDynamicCaracteristicasPanelForm("PR_CONSULTA_PREDIO",
                                                          null);
    }

    public RichPanelFormLayout getCaracteristicasAsentamientosPanel() {
        return this.createDynamicCaracteristicasPanelForm("PR_CONSULTA_ASENTAMIENTO",
                                                          null);
    }

    public void setPnlCaractCompTerreno(RichPanelFormLayout pnlCaractCompTerreno) {
        this.pnlCaractCompTerreno = pnlCaractCompTerreno;

    }

    public void setPnlCaractCompConstruccion(RichPanelFormLayout pnlCaractCompConstruccion) {
        this.pnlCaractCompConstruccion = pnlCaractCompConstruccion;
    }

    public RichPanelFormLayout getPnlCaractCompConstruccion() {
        return this.pnlCaractCompConstruccion;
    }

    public RichPanelFormLayout getPnlCaractCompTerreno() {
        return this.pnlCaractCompTerreno;
    }

    public void setTblOtrosPredios(RichTable tblOtrosPredios) {
        this.tblOtrosPredios = tblOtrosPredios;
    }

    public RichTable getTblOtrosPredios() {
        return tblOtrosPredios;
    }

    /*
     * Prepare Model
     */

    public void prepareModel(LifecycleContext context) {
        super.prepareModel(context);
        /*RequestContext rc = RequestContext.getCurrentInstance();
        if (!rc.isPostback()) {
            if (this.getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
                Map pFScope = getProcessBean();
                Predio predio = (Predio)pFScope.get(JavaBeanKeys.PR_PREDIO);
                Integer predSerie = predio.getObjeSerie();
                Long predId = predio.getObjeIdentificador();
                String estatus = predio.getEstatus();
                //Otos due�os
                //getOtrosDuenios(predSerie, predId);
                //Colindancias
                //getColindanciasPredio(predSerie, predId);
                //Valores del predio
                //getValoresPredio(predSerie,predId);
                //�ltimo moviemiento
                //getUltimoMovimiento(predSerie, predId);
                //Secci�n de "�ltimo pago"
                //getUltimoPago(predSerie, predId, estatus);
            }
        }*/
    }

    public String operacionAnterior() {
        String trancision = "busqueda";
        return trancision;
    }

    /*
     * M�todo para el despliegue de las tabs
     */

    public void despliegaTabs(DisclosureEvent disclosureEvent) {
        BindingContainer bindings = getBindings();
        Predio predio = new Predio();
        if (getShowDetailItemDatosPredios().isDisclosed()) {
            //Secci�n de datos de predio
            getDatos();
            getUltimoPago();
        } else if (getShowDetailItemCaractTerreno().isDisclosed()) {
            if (this.getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
                //FILTRADO DE LOS COMPONENTES DE TIPO TERRENO
                predio =
                        (Predio)this.getProcessBean().get(JavaBeanKeys.PR_PREDIO);
                Integer prPredSerie = predio.getObjeSerie();
                Long prPredIdentificador = predio.getObjeIdentificador();
                filtrarComponentesPredio("TE", prPredSerie,
                                         prPredIdentificador);
                RichPanelFormLayout pnlTerreno = getPnlCaractCompTerreno();
                pnlTerreno.getChildren().clear();
            }
        } else if (getShowDetailItemCaractConst().isDisclosed()) {
            if (this.getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
                //FILTRADO DE LOS COMPONENTES DE TIPO CONSTRUCCI�N
                predio =
                        (Predio)this.getProcessBean().get(JavaBeanKeys.PR_PREDIO);
                Integer prPredSerie = predio.getObjeSerie();
                Long prPredIdentificador = predio.getObjeIdentificador();
                filtrarComponentesPredio("CO", prPredSerie,
                                         prPredIdentificador);
                RichPanelFormLayout pnlConstruccion =
                    getPnlCaractCompConstruccion();
                pnlConstruccion.getChildren().clear();
            }
        } else if (getShowDetailItemHistMov().isDisclosed()) {
            //Historial de movimientos
            if (this.getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
                predio =
                        (Predio)this.getProcessBean().get(JavaBeanKeys.PR_PREDIO);
                Integer prPredSerie = predio.getObjeSerie();
                Long prPredIdentificador = predio.getObjeIdentificador();
                getHistorialMovimiento(prPredSerie, prPredIdentificador);
            }
        } else if (getShowDetailItemObligaciones().isDisclosed()) {
            System.out.println("Obligaciones");
        } else if (getShowDetailItemOtrosPredios().isDisclosed()) {
            if (this.getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
                predio =
                        (Predio)this.getProcessBean().get(JavaBeanKeys.PR_PREDIO);
                Long noPersona = predio.getPropietario().getPropietario().getNoPersona();
                getOtrosPredios(noPersona);
            }
            //Prueba para poblar el objeto predio. ELIMINAR CUANDO ESTE LISTO TOD0.
            /*OperationBinding ob =
              (OperationBinding)bindings.getOperationBinding("getOtrosPrediosPorPersona");
          ob.getParamsMap().put("noPersona", 8);
          ob.execute();*/
        }
    }

    /*
     * Secci�n de Datos del predio
     */

    private void getDatos() {
        if (getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
            Predio predio =
                (Predio)getProcessBean().get(JavaBeanKeys.PR_PREDIO);
            Integer predSerie = predio.getObjeSerie();
            Long predId = predio.getObjeIdentificador();
            String estatus = predio.getEstatus();
            //Otos due�os
            getOtrosDuenios(predSerie, predId);
            //Colindancias
            getColindanciasPredio(predSerie, predId);
            //Valores del predio
            //getValoresPredio(predSerie,predId);
            //�ltimo moviemiento
            getUltimoMovimiento(predSerie, predId);
            //Secci�n de "�ltimo pago"
            getUltimoPago(predSerie, predId, estatus);
        }
    }

    private void getUltimoPago() {
        BindingContainer bindings = getBindings();
        if (getProcessBean().containsKey(JavaBeanKeys.PR_PREDIO)) {
            Predio predio =
                (Predio)getProcessBean().get(JavaBeanKeys.PR_PREDIO);
            OperationBinding ob =
                (OperationBinding)bindings.getOperationBinding("getUltimoPagoPredio");
            ob.getParamsMap().put("pagoPredSerie", predio.getObjeSerie());
            ob.getParamsMap().put("pagoPredId", predio.getObjeIdentificador());
            ob.getParamsMap().put("Estatus", predio.getEstatus());
            ob.execute();
        }
    }

    private void getOtrosDuenios(Integer predSerie, Long predId) {
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            (OperationBinding)bindings.getOperationBinding("getOtrosDueniosPredio");
        ob.getParamsMap().put("predSerie", predSerie);
        ob.getParamsMap().put("predId", predId);
        ob.execute();
    }

    private void getColindanciasPredio(Integer predSerie, Long predId) {
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            (OperationBinding)bindings.getOperationBinding("getColindanciasPredio");
        ob.getParamsMap().put("predSerie", predSerie);
        ob.getParamsMap().put("predId", predId);
        ob.getParamsMap().put("Estatus", "AC");
        ob.execute();
    }

    private void getUltimoMovimiento(Integer predSerie, Long predId) {
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            (OperationBinding)bindings.getOperationBinding("getUltimoMovimientoPredio");
        ob.getParamsMap().put("predSerie", predSerie);
        ob.getParamsMap().put("predId", predId);
        ob.execute();
    }

    private void getUltimoPago(Integer predSerie, Long predId,
                               String estatus) {
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            (OperationBinding)bindings.getOperationBinding("getUltimoPagoPredio");
        ob.getParamsMap().put("pagoPredSerie", predSerie);
        ob.getParamsMap().put("pagoPredId", predId);
        ob.getParamsMap().put("Estatus", estatus);
        ob.execute();
    }

    /*
     * Secci�n de Caracter�sticas de componentes
     */

    public void caractTerrenoActionListener(ActionEvent actionEvent) {
        try {
            AttributeBinding idTerreno =
                (AttributeBinding)this.getBindings().getControlBinding("selectedRow");
            String id = idTerreno.getInputValue().toString();
            RichPanelFormLayout pnlTerreno = getPnlCaractCompTerreno();
            UIComponent caractTerrenos =
                this.createDynamicCaracteristicasPanelForm(JavaBeanKeys.PR_CONSULTA_TERRENO,
                                                           Integer.parseInt(id));
            pnlTerreno.getChildren().clear();
            pnlTerreno.getChildren().add(caractTerrenos);
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
    }

    public void caractConstruccionActionListener(ActionEvent actionEvent) {
        try {
            AttributeBinding idConstruccion =
                (AttributeBinding)this.getBindings().getControlBinding("selectedRow");
            String id = idConstruccion.getInputValue().toString();
            RichPanelFormLayout pnlConstruccion =
                getPnlCaractCompConstruccion();
            UIComponent caractConstruccion =
                this.createDynamicCaracteristicasPanelForm(JavaBeanKeys.PR_CONSULTA_CONSTRUCCION,
                                                           Integer.parseInt(id));
            pnlConstruccion.getChildren().clear();
            pnlConstruccion.getChildren().add(caractConstruccion);
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
        }
    }

    private void filtrarComponentesPredio(String tipoComponente, Integer serie,
                                          Long id) {
        OperationBinding ob =
            (OperationBinding)this.getBindings().getOperationBinding("getPredComponentes");
        ob.getParamsMap().put("PrPredSerie", serie);
        ob.getParamsMap().put("PrPredIdentificador", id);
        ob.getParamsMap().put("TipoComponente", tipoComponente);
        ob.execute();
    }

    /*
     * Secci�n de Historial de Movimientos
     */

    private void getHistorialMovimiento(Integer predSerie, Long predId) {
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            (OperationBinding)bindings.getOperationBinding("getHistorialMovimientoPredio");
        ob.getParamsMap().put("predSerie", predSerie);
        ob.getParamsMap().put("predId", predId);
        ob.execute();
    }

    /*
     * Secci�n de Obligaciones
     */

    /*
     * Secci�n de Otros Predios
     */

    private void getOtrosPredios(Long noPersona) {
        BindingContainer bindings = getBindings();
        OperationBinding ob =
            (OperationBinding)bindings.getOperationBinding("getOtrosPrediosPorPersona");
        ob.getParamsMap().put("noPersona", noPersona);
        ob.execute();
    }

    /*
     * Auxiliares
     */

    private Map getProcessBean() {
        AdfFacesContext processBean;
        processBean = AdfFacesContext.getCurrentInstance();
        Map beanSessionMap = processBean.getPageFlowScope();
        return beanSessionMap;
    }

    private RichPanelFormLayout createDynamicCaracteristicasPanelForm(String bloque,
                                                                      Integer componente) {
        //this.getProcessBean().remove(JavaBeanKeys.BLOQUES_PR_CONSULTA);
        //Estos ser�n los valores que se mandan al m�todo de persistencia.
        String nombreBloque = bloque;
        Integer objeSerie = null;
        Long objeId = null;
        Integer idComponente = null;
        if (componente != null) {
            idComponente = componente;
        }
        FacesContext fc = FacesContext.getCurrentInstance();
        RichPanelFormLayout cpf =
            (RichPanelFormLayout)fc.getApplication().createComponent(RichPanelFormLayout.COMPONENT_TYPE);
        //Es el m�todo que hizo luis. Se tendr�a que recibir como par�metro u obtener del Predio global
        Predio predio = (Predio)getProcessBean().get(JavaBeanKeys.PR_PREDIO);
        Integer predSerie = predio.getObjeSerie();
        Long predId = predio.getObjeIdentificador();
        objeSerie = predSerie;
        objeId = predId;
        //Llamado al m�todo de Luis
        List bloques =
            getBloquesCaracteristicas(nombreBloque, objeSerie, objeId,
                                      idComponente);
        for (int i = 0; i < bloques.size(); i++) {
            PredioBloqueAgrupacion bloqueAgrupacion;
            bloqueAgrupacion = (PredioBloqueAgrupacion)bloques.get(i);
            RichPanelHeader table = crearTabla(bloqueAgrupacion);
            RichSpacer spacer = new RichSpacer();
            spacer.setHeight("10");
            cpf.getChildren().add(table);
            cpf.getChildren().add(spacer);
        }
        if (bloques.size() == 0) {
            RichPanelLabelAndMessage rplm = new RichPanelLabelAndMessage();
            //TODO: resource bundle
            rplm.setLabel("No se encuentran bloques de caracter�sticas");
            cpf.getChildren().add(rplm);
        }
        return cpf;
    }

    /**
     *Consulta de los bloques seg�n el movimiento
     * @param nombre
     * @return List con objetos tipos BloqueCaracteristica
     */
    private List getBloquesCaracteristicas(String nombre, Integer serie,
                                           Long identificador,
                                           Integer idComp) {
        oracle.binding.OperationBinding operationBinding =
            getBindings().getOperationBinding("getBloquesCaracteristicas");
        operationBinding.getParamsMap().put("nombre", nombre);
        operationBinding.getParamsMap().put("serie", serie);
        operationBinding.getParamsMap().put("identificador", identificador);
        operationBinding.getParamsMap().put("idComp", idComp);
        List result = (List)operationBinding.execute();
        return result;
    }

    private RichPanelHeader crearTabla(PredioBloqueAgrupacion bloque) {
        FacesContext fc = FacesContext.getCurrentInstance();

        RichPanelHeader table = new RichPanelHeader();
        // ============================= T�tulo ===============================
        table.setText(bloque.getEtiqueta());
        //=========================== Caracter�sticas ============================

        //Panel para alineaci�n de componentes.
        RichPanelFormLayout panel = new RichPanelFormLayout();
        panel.setLabelAlignment(RichPanelFormLayout.LABEL_ALIGNMENT_START);

        if (bloque.getPredialCaracteristicas() != null) {

            for (int i = 0; i < bloque.getPredialCaracteristicas().size();
                 i++) {
                PredialCaracteristica ac =
                    (PredialCaracteristica)bloque.getPredialCaracteristicas().get(i);

                String detalle1 = null;
                String detalle2 = null;
                String detalle3 = null;
                String detalle4 = null;

                if (ac.getValorCaracteristica() != null) {
                    detalle1 = ac.getValorCaracteristica().getValor1();
                    detalle2 = ac.getValorCaracteristica().getValor2();
                    detalle3 = ac.getValorCaracteristica().getValor3();
                    detalle4 = ac.getValorCaracteristica().getValor4();
                }

                // Caracteristicas con comportamiento R (Doble captura)
                if (ac.getComportamiento() == 'R') {
                    RichInputText rit1 = new RichInputText();
                    RichInputText rit2 = new RichInputText();
                    if (ac.getPresentacionCaracteristica() != null) {
                        if (ac.getPresentacionCaracteristica().getCapturable().equals("N")) {
                            rit1.setReadOnly(true);
                            rit2.setReadOnly(true);
                        }
                    }

                    rit1.setLabel(ac.getEtiqueta() + ":");
                    rit1.setMaximumLength((int)ac.getLongitudMaxima());
                    rit1.setColumns((int)ac.getLongitudDespliegue());
                    rit2.setMaximumLength((int)ac.getLongitudMaxima());
                    rit2.setColumns((int)ac.getLongitudDespliegue());
                    
                    if (ac.getRequerida().equals("S")) {
                        rit1.setRequired(true);
                        rit2.setRequired(true);
                    }
                    if (detalle1 != null) {
                        rit1.setValue(detalle1);
                    } else {
                        rit1.setValue("");
                    }
                    if (detalle2 != null) {
                        rit2.setValue(detalle2);
                    } else {
                        rit2.setValue("");
                    }
                    rit1.setId("i_" + ac.getIdentificador() + "_" +
                               bloque.getIdentificador().toString());
                    rit2.setId("i_" + ac.getIdentificador() +
                               "_2_" + bloque.getIdentificador().toString());
                    rit1.setParent(table);
                    rit2.setParent(table);
                    setValidatorConverter(rit1, ac);
                    setValidatorConverter(rit2, ac);
                    panel.getChildren().add(rit1);
                    panel.getChildren().add(rit2);
                }

                // Caracteristicas con comportamiento Sencillo
                else if (ac.getComportamiento() == 'S') {
                    RichInputText rit1 = new RichInputText();

                    if (ac.getPresentacionCaracteristica() != null) {
                        if (ac.getPresentacionCaracteristica().getCapturable().equals("N")) {
                            rit1.setReadOnly(true);
                        }
                    }
                    rit1.setColumns((int)ac.getLongitudDespliegue());
                    rit1.setMaximumLength((int)ac.getLongitudMaxima());
                    // Barra de desplazamiento
                    if (ac.getCampo1() != null) {
                        rit1.setRows(Integer.parseInt(ac.getCampo1()));
                    }
                    if (ac.getRequerida().equals("S")) {
                        rit1.setRequired(true);
                    }
                    if (ac.getTipoDeValidacion().equals("FL")) {
                        rit1.setStyleClass("AFFieldNumber");
                    } else if (ac.getTipoDeValidacion().equals("IN")) {
                        rit1.setStyleClass("AFFieldNumber");
                    } else {
                        System.out.println("Validacion Libre");
                    }
                    if (detalle1 != null) {
                        rit1.setValue(detalle1);
                    } else {
                        rit1.setValue("");
                    }
                    rit1.setLabel(ac.getEtiqueta() + ":");
                    rit1.setId("i_" + ac.getIdentificador() + "_" +
                               bloque.getIdentificador().toString());
                    rit1.setParent(table);
                    setValidatorConverter(rit1, ac);
                    panel.getChildren().add(rit1);
                }

                // Caracteristicas con comportamiento de Lista de valores
                else if (ac.getComportamiento() == 'L') {
                    RichSelectOneChoice rsoc = new RichSelectOneChoice();
                    if (ac.getPresentacionCaracteristica() != null) {
                        if (ac.getPresentacionCaracteristica().getCapturable().equals("N")) {
                            rsoc.setReadOnly(true);
                        }
                    }
                    rsoc.setLabel(ac.getEtiqueta() + ":");
                    if (ac.getRequerida().equals("S")) {
                        rsoc.setRequired(true);
                    }
                    rsoc.setId("i_" + ac.getIdentificador() + "_" +
                               bloque.getIdentificador().toString());
                    LovBean lv = new LovBean();
                    List lov =
                        (List)lv.getListaValoresList().get("PR." + ac.getNombre());
                
                    if (detalle1 != null) {
                        rsoc.setValue(detalle1);
                    } else {
                        rsoc.setValue("");
                    }
                    UISelectItems usi = new UISelectItems();
                    usi.setValue(lov);
                    setValidatorConverter(rsoc, ac);
                    rsoc.getChildren().add(usi);
                    panel.getChildren().add(rsoc);
                } else if (ac.getComportamiento() == 'B') {
                    // declaramos el componente que tendr� el mensaje configurado
                    // en la caracteristica.
                    /*    RichPanelLabelAndMessage rplm =
                        new RichPanelLabelAndMessage();
                    rplm.setLabel(ac.getEtiqueta() + ":");
                    rplm.setShowRequired(true);
                */ // y el list con los items.
                    //  RichPanelGroupLayout rpg = new RichPanelGroupLayout();
                    //  rpg.setLayout(RichPanelGroupLayout.LAYOUT_VERTICAL);
                    // Declaramos el list con los valores
                    /*  HtmlTableLayout htl = new HtmlTableLayout();
                    HtmlRowLayout hrl = new HtmlRowLayout();
                    HtmlCellFormat hcl1 = new HtmlCellFormat();
                    htl.getChildren().add(hrl);
                    hrl.getChildren().add(hcl1);
                    htl.setStyleClass("af_panelBox_light af_table_content");
                    htl.setWidth("20%");
                    RichSelectOneListbox rsoc = new RichSelectOneListbox();
                  /*if (ac.getRequerida().equals("S")) {
                        rsoc.setRequired(true);
                }*/
                    /* rsoc.setSize(6);
                    rsoc.setId("i_" + ac.getIdentificador().toString() + "_" +
                               bloque.getIdentificador().toString());
                  LovBean lv = new LovBean();
                  List lov =
                      (List)lv.getListaValoresList().get("PR." + ac.getNombre());

                  if (detalle1 != null) {
                      rsoc.setValue(detalle1);
                    } else {
                        rsoc.setValue("");
                    }
                    UISelectItems usi = new UISelectItems();
                    // usi.setValue(lov);
                    rsoc.getChildren().add(usi);
                    rsoc.setSimple(true);
                    // ahora declaramos el inputText que servir� para capturar
                    // la busqueda y lo agregamos al group
                    RichInputText rit = new RichInputText();
                    rit.setColumns(50);
                    rit.setShortDesc("Solo acepta un comod�n al principio. No " +
                                     "es sensitivo entre may�suclas y min�sculas. Se " +
                                     "selecciona la primer opci�n y se ilumina el resto de " +
                                     "las coincidencias.");
                    rpg.getChildren().add(rit);
                    rpg.getChildren().add(rsoc);
                    hcl1.getChildren().add(rpg);
                    rplm.getChildren().add(htl);

                    panel.getChildren().add(rplm);
  */
                }

                ///PARA EL INPUTDATE
                else if (ac.getComportamiento() == 'F') {
                    // creamos el componente
                    RichInputDate rsid = new RichInputDate();
                    if (ac.getRequerida().equals("S")) {
                        rsid.setRequired(true);
                    }
                    if (ac.getPresentacionCaracteristica() != null) {
                        if (ac.getPresentacionCaracteristica().getCapturable().equals("N")) {
                            rsid.setReadOnly(true);
                        }
                    }
        
                    if (detalle1 != null) {
                        rsid.setValue(detalle1);
                    } else {
                        rsid.setValue("");
                    }

                    rsid.setLabel(ac.getEtiqueta() + ":");
                    rsid.setId("i_" + ac.getIdentificador() + "_" +
                               bloque.getIdentificador().toString());
                    rsid.setParent(table);
                    setValidatorConverter(rsid, ac);
                    panel.getChildren().add(rsid);
                }

                //Para el checkbox
                else if (ac.getComportamiento() == 'C') {
                    RichSelectBooleanCheckbox rsbc =
                        new RichSelectBooleanCheckbox();
                    if (ac.getPresentacionCaracteristica() != null) {
                        if (ac.getPresentacionCaracteristica().getCapturable().equals("N")) {
                            rsbc.setReadOnly(true);
                        }
                    }
                    if (ac.getRequerida().equals("S")) {
                        rsbc.setRequired(true);
                    }
                  
                    if (detalle1 != null) {
                        if (detalle1.equals("TRUE")) {
                            rsbc.setSelected(true);
                        } else {
                            rsbc.setSelected(false);
                        }
                    } else {
                        rsbc.setSelected(false);
                    }
                    rsbc.setLabel(ac.getEtiqueta() + ":");
                    rsbc.setId("i_" + ac.getIdentificador() + "_" +
                               bloque.getIdentificador().toString());
                    rsbc.setParent(table);
                    setValidatorConverter(rsbc, ac);
                    panel.getChildren().add(rsbc);
                }
                // Caracteristicas con comportamiento de lista de valores doble
                table.getChildren().add(panel);
                table.setStyleClass("AFStretchWith AFStretchHeight");

                indice++;
            }


            // si el bloque tiene un bloque anidado
            if (bloque.getPredioBloqueAgrupacion() != null) {
                for (int hijo = 0;
                     hijo < bloque.getPredioBloqueAgrupacion().size();
                     hijo++) {

                    table.getChildren().add(this.crearTabla(bloque.getPredioBloqueAgrupacion().get(hijo)));
                }
            }
        }

        return table;
    }

    private void setValidatorConverter(UIXEditableValue com,
                                       PredialCaracteristica o) {
        String[] validator = null;
        String[] converter = null;

        //Validator
        if (o.getCampo2() != null) {
            validator = o.getCampo2().split(",");
            for (int v = 0; v < validator.length; v++) {
                if (validator[v].equals(ValidatorKeys.FECHA_MAYOR_ACTUAL)) {
                    com.addValidator(new FechaMayorActual());
                }
            }
        }
        
        //Converter
        if (o.getCampo3() != null) {
            converter = o.getCampo3().split(",");
            for (int c = 0; c < converter.length; c++) {
                if (converter[c].equals(ConverterKeys.MAYUS)) {
                    com.setConverter(new UpperCaseConverter());
                } else if (converter[c].equals(ConverterKeys.FECHA)) {
                    DateTimeConverter dateConverter = new DateTimeConverter();
                    dateConverter.setPattern("dd-MM-yyyy");
                    com.setConverter(dateConverter);
                }
            }
        }
    }

    public void cargarPredioActionListener(ActionEvent actionEvent) {
        JUCtrlHierNodeBinding rowData =
            (JUCtrlHierNodeBinding)getTblOtrosPredios().getSelectedRowData();
        Row row = rowData.getCurrentRow();
        Integer nuevaSerie =
            Integer.parseInt(row.getAttribute("Serie").toString());
        Long nuevaId =
            Long.parseLong(row.getAttribute("Identificador").toString());
        BindingContainer bindings = this.getBindings();
        OperationBinding datosPredio =
            (OperationBinding)bindings.getOperationBinding("getDatosPredio");
        Map pFScope = this.getProcessBean();
        pFScope.remove(JavaBeanKeys.PR_PREDIO);
        datosPredio.getParamsMap().put("predSerie", nuevaSerie);
        datosPredio.getParamsMap().put("predId", nuevaId);
        Predio predio = (Predio)datosPredio.execute();
        if (datosPredio.getErrors().isEmpty()) {
            pFScope.put(JavaBeanKeys.PR_PREDIO, predio);
            DomicilioNotificar dom = null;
            String domicilioRef = "";
            if (predio.getPropietario().getPropietario() != null) {
                dom =
predio.getPropietario().getPropietario().getDomicilio();
                domicilioRef =
                        dom.getTipoCalle() + " " + dom.getCalle() + " " +
                        dom.getNumeroExterior() + " " + dom.getLetra() + " " +
                        dom.getNumeroInterior() + " " + dom.getColonia() +
                        " " + dom.getCodigoPostal() + " " +
                        dom.getLocalidad() + " " + dom.getMunicipio() + " " +
                        dom.getEstado();
                pFScope.put("domicilioRef", domicilioRef);
            } else if (predio.getPropietario().getPropietario() != null) {
                dom =
predio.getPropietario().getPropietario().getDomicilio();
                domicilioRef =
                        dom.getTipoCalle() + " " + dom.getCalle() + " " +
                        dom.getNumeroExterior() + " " + dom.getLetra() + " " +
                        dom.getNumeroInterior() + " " + dom.getColonia() +
                        " " + dom.getCodigoPostal() + " " +
                        dom.getLocalidad() + " " + dom.getMunicipio() + " " +
                        dom.getEstado();
                pFScope.put("domicilioRef", domicilioRef);
            }
            this.mostrarMensaje(FacesMessage.SEVERITY_INFO,
                                properties.getMessage("PREDIO_CARGADO_EXITO"));
        } else {
            String msj = datosPredio.getErrors().get(0).toString();
            FacesContext.getCurrentInstance().addMessage(null,
                                                         new FacesMessage(msj));
        }
    }
}
