function FieldAsUppercase()
{
  // for debugging
  this._class = "FieldAsUppercase";
}

FieldAsUppercase.prototype = new TrConverter();

FieldAsUppercase.prototype.getFormatHint = function()
{
  return null;
}

FieldAsUppercase.prototype.getAsString = function(string,label)
{
  return string.toUpperCase();
}

FieldAsUppercase.prototype.getAsObject = function(string,label)
{
  return string.toUpperCase();
}