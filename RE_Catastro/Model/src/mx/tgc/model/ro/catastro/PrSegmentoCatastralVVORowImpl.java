package mx.tgc.model.ro.catastro;

import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Aug 12 11:03:15 MDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PrSegmentoCatastralVVORowImpl extends ViewRowImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        Serie {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getSerie();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setSerie((Number)value);
            }
        }
        ,
        Identificador {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getIdentificador();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setIdentificador((Number)value);
            }
        }
        ,
        ClaveSegmento {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getClaveSegmento();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setClaveSegmento((String)value);
            }
        }
        ,
        Descripcion {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getDescripcion();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setDescripcion((String)value);
            }
        }
        ,
        TipoSegmento {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getTipoSegmento();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setTipoSegmento((String)value);
            }
        }
        ,
        Nivel {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getNivel();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setNivel((Number)value);
            }
        }
        ,
        Estatus {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getEstatus();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setEstatus((String)value);
            }
        }
        ,
        PcMuniIdentificador {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getPcMuniIdentificador();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setPcMuniIdentificador((Number)value);
            }
        }
        ,
        PrSecaSerie {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getPrSecaSerie();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setPrSecaSerie((Number)value);
            }
        }
        ,
        PrSecaIdentificador {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getPrSecaIdentificador();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setPrSecaIdentificador((Number)value);
            }
        }
        ,
        Level {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getLevel();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setLevel((Number)value);
            }
        }
        ,
        ClaveCatastral {
            public Object get(PrSegmentoCatastralVVORowImpl obj) {
                return obj.getClaveCatastral();
            }

            public void put(PrSegmentoCatastralVVORowImpl obj, Object value) {
                obj.setClaveCatastral((String)value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(PrSegmentoCatastralVVORowImpl object);

        public abstract void put(PrSegmentoCatastralVVORowImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int SERIE = AttributesEnum.Serie.index();
    public static final int IDENTIFICADOR = AttributesEnum.Identificador.index();
    public static final int CLAVESEGMENTO = AttributesEnum.ClaveSegmento.index();
    public static final int DESCRIPCION = AttributesEnum.Descripcion.index();
    public static final int TIPOSEGMENTO = AttributesEnum.TipoSegmento.index();
    public static final int NIVEL = AttributesEnum.Nivel.index();
    public static final int ESTATUS = AttributesEnum.Estatus.index();
    public static final int PCMUNIIDENTIFICADOR = AttributesEnum.PcMuniIdentificador.index();
    public static final int PRSECASERIE = AttributesEnum.PrSecaSerie.index();
    public static final int PRSECAIDENTIFICADOR = AttributesEnum.PrSecaIdentificador.index();
    public static final int LEVEL = AttributesEnum.Level.index();
    public static final int CLAVECATASTRAL = AttributesEnum.ClaveCatastral.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PrSegmentoCatastralVVORowImpl() {
    }

    /**
     * Gets the attribute value for the calculated attribute Serie.
     * @return the Serie
     */
    public Number getSerie() {
        return (Number) getAttributeInternal(SERIE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Serie.
     * @param value value to set the  Serie
     */
    public void setSerie(Number value) {
        setAttributeInternal(SERIE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Identificador.
     * @return the Identificador
     */
    public Number getIdentificador() {
        return (Number) getAttributeInternal(IDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Identificador.
     * @param value value to set the  Identificador
     */
    public void setIdentificador(Number value) {
        setAttributeInternal(IDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ClaveSegmento.
     * @return the ClaveSegmento
     */
    public String getClaveSegmento() {
        return (String) getAttributeInternal(CLAVESEGMENTO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ClaveSegmento.
     * @param value value to set the  ClaveSegmento
     */
    public void setClaveSegmento(String value) {
        setAttributeInternal(CLAVESEGMENTO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Descripcion.
     * @return the Descripcion
     */
    public String getDescripcion() {
        return (String) getAttributeInternal(DESCRIPCION);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Descripcion.
     * @param value value to set the  Descripcion
     */
    public void setDescripcion(String value) {
        setAttributeInternal(DESCRIPCION, value);
    }

    /**
     * Gets the attribute value for the calculated attribute TipoSegmento.
     * @return the TipoSegmento
     */
    public String getTipoSegmento() {
        return (String) getAttributeInternal(TIPOSEGMENTO);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute TipoSegmento.
     * @param value value to set the  TipoSegmento
     */
    public void setTipoSegmento(String value) {
        setAttributeInternal(TIPOSEGMENTO, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Nivel.
     * @return the Nivel
     */
    public Number getNivel() {
        return (Number) getAttributeInternal(NIVEL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Nivel.
     * @param value value to set the  Nivel
     */
    public void setNivel(Number value) {
        setAttributeInternal(NIVEL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Estatus.
     * @return the Estatus
     */
    public String getEstatus() {
        return (String) getAttributeInternal(ESTATUS);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Estatus.
     * @param value value to set the  Estatus
     */
    public void setEstatus(String value) {
        setAttributeInternal(ESTATUS, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PcMuniIdentificador.
     * @return the PcMuniIdentificador
     */
    public Number getPcMuniIdentificador() {
        return (Number) getAttributeInternal(PCMUNIIDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PcMuniIdentificador.
     * @param value value to set the  PcMuniIdentificador
     */
    public void setPcMuniIdentificador(Number value) {
        setAttributeInternal(PCMUNIIDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PrSecaSerie.
     * @return the PrSecaSerie
     */
    public Number getPrSecaSerie() {
        return (Number) getAttributeInternal(PRSECASERIE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PrSecaSerie.
     * @param value value to set the  PrSecaSerie
     */
    public void setPrSecaSerie(Number value) {
        setAttributeInternal(PRSECASERIE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute PrSecaIdentificador.
     * @return the PrSecaIdentificador
     */
    public Number getPrSecaIdentificador() {
        return (Number) getAttributeInternal(PRSECAIDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute PrSecaIdentificador.
     * @param value value to set the  PrSecaIdentificador
     */
    public void setPrSecaIdentificador(Number value) {
        setAttributeInternal(PRSECAIDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Level.
     * @return the Level
     */
    public Number getLevel() {
        return (Number) getAttributeInternal(LEVEL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Level.
     * @param value value to set the  Level
     */
    public void setLevel(Number value) {
        setAttributeInternal(LEVEL, value);
    }

    /**
     * Gets the attribute value for the calculated attribute ClaveCatastral.
     * @return the ClaveCatastral
     */
    public String getClaveCatastral() {
        return (String) getAttributeInternal(CLAVECATASTRAL);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute ClaveCatastral.
     * @param value value to set the  ClaveCatastral
     */
    public void setClaveCatastral(String value) {
        setAttributeInternal(CLAVECATASTRAL, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value, AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }
}
