package mx.tgc.model.ro.ubicacion;

import java.util.List;

import mx.tgc.model.ro.ubicacion.common.PcAsentamientosVVO;
import mx.tgc.utilityfwk.model.extension.view.GenericViewImpl;
import mx.tgc.utilityfwk.model.extension.view.search.Filterable;

import mx.tgc.utilityfwk.view.managed.GenericBean;

// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Thu Aug 18 15:50:58 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PcAsentamientosVVOImpl extends GenericViewImpl implements Filterable,
                                                                       PcAsentamientosVVO {
    /**
     * This is the default constructor (do not remove).
     */
   

    public PcAsentamientosVVOImpl() {
    }

    public void filtrarVista(List list) {
        super.buscar(list);
    }


}
