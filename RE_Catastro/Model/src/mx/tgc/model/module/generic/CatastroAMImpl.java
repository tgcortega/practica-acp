package mx.tgc.model.module.generic;

import mx.tgc.model.ro.ubicacion.AaUsuariosMunicipiosVVOImpl;
import mx.tgc.utilityfwk.model.extension.module.GenericAMImpl;

import oracle.jbo.ViewCriteria;
import oracle.jbo.ViewCriteriaManager;
import oracle.jbo.ViewCriteriaRow;
import oracle.jbo.server.ViewObjectImpl;


public class CatastroAMImpl extends GenericAMImpl {
    public CatastroAMImpl() {
        super();
    }
    
    public void filtrarPorMunicipio(String view, String muniId, Boolean nullCase) {
        ViewObjectImpl vo = (ViewObjectImpl)this.findViewObject(view);
        ViewCriteriaManager vcm = vo.getViewCriteriaManager();
        ViewCriteria myViewCriteria;
        ViewCriteriaRow vcr1;
        if (vcm.getApplyViewCriteriaNames() != null && vcm.getApplyViewCriteriaNames().length != 0) {
            //cuando ya trae un view criteria
            myViewCriteria = vcm.getViewCriteria(vcm.getApplyViewCriteriaNames()[0]);
            vcr1 = (ViewCriteriaRow)myViewCriteria.getRowAtRangeIndex(0);
        } else {
            myViewCriteria = vo.createViewCriteria();
            vcr1 = myViewCriteria.createViewCriteriaRow();
            myViewCriteria.add(vcr1);
            vo.applyViewCriteria(myViewCriteria);
        }
        vcr1.setAttribute("PcMuniIdentificador", "IN (" + muniId + ")");
        vo.executeQuery();
        if (nullCase && vo.getRowCount() == 0) {
            vcr1.setAttribute("PcMuniIdentificador", "is null");
            vo.executeQuery();
        }
        if (muniId == null && vo.getRowCount() == 0) {
            vo.applyViewCriteria(null);
            vo.executeQuery();
        }
    }

    public void filtrarPorRecaudacion(String view, Number idRecaudacion,
                                      Boolean nullCase) {
        ViewObjectImpl vo = (ViewObjectImpl)this.findViewObject(view);
        ViewCriteriaManager vcm = vo.getViewCriteriaManager();
        ViewCriteria myViewCriteria;
        ViewCriteriaRow vcr1;
        if (vcm.getApplyViewCriteriaNames() != null && 
            vcm.getApplyViewCriteriaNames().length != 0) {
            //cuando ya trae un view criteria
            myViewCriteria =
                    vcm.getViewCriteria(vcm.getApplyViewCriteriaNames()[0]);
            vcr1 = (ViewCriteriaRow)myViewCriteria.getRowAtRangeIndex(0);
        } else {
            myViewCriteria = vo.createViewCriteria();
            vcr1 = myViewCriteria.createViewCriteriaRow();
            myViewCriteria.add(vcr1);
            vo.applyViewCriteria(myViewCriteria);
        }
        vcr1.setAttribute("CcRecaIdentificador", "=" + idRecaudacion);
        vo.executeQuery();
        if (nullCase && vo.getRowCount() == 0) {
            vcr1.setAttribute("CcRecaIdentificador", "is null");
            vo.executeQuery();
        }
    }
    /**
     * Se filtra la vista dependiendo si es llamada como un popup o 
     * como catalogo
     * @param filtrar
     * @author Manuel Armendariz
     */
    public void filtrarActivos(Boolean filtrar, String view){
        ViewObjectImpl vo = (ViewObjectImpl)this.findViewObject(view);
        if (filtrar){
            vo.setApplyViewCriteriaName("filtrarActivos");
        } else {
            vo.removeApplyViewCriteriaName("filtrarActivos");
        }
        vo.executeQuery();
    }
}
