package mx.tgc.model.entity.ubicacion;


import mx.net.tgc.util.FechasUtil;

import mx.tgc.model.ro.ubicacion.PcLocalidadesVVORowImpl;
import mx.tgc.utilityfwk.model.extension.entity.GenericEntityImpl;

import oracle.jbo.AttributeList;
import oracle.jbo.Key;
import oracle.jbo.RowIterator;
import oracle.jbo.domain.DBSequence;
import oracle.jbo.domain.Date;
import oracle.jbo.domain.Number;
import oracle.jbo.server.AttributeDefImpl;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.SequenceImpl;
import oracle.jbo.server.TransactionEvent;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Aug 19 11:23:54 MDT 2011
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class PcVialidadesEOImpl extends GenericEntityImpl {

    @Override
    protected void initDefaults() {
        super.initDefaults();
        SequenceImpl seq = new SequenceImpl("PC_VIAL_SEQ",getDBTransaction());
        DBSequence dbseq = new DBSequence(seq.getSequenceNumber());        
        populateAttributeAsChanged(IDENTIFICADOR,dbseq.getSequenceNumber());
    }

    @Override
    public void afterCommit(TransactionEvent transactionEvent) {
        super.afterCommit(transactionEvent);
    }


    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. Do not modify.
     */
    public enum AttributesEnum {
        Identificador {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getIdentificador();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setIdentificador((Number)value);
            }
        }
        ,
        ClaveVialidad {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getClaveVialidad();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setClaveVialidad((String)value);
            }
        }
        ,
        TipoVialidad {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getTipoVialidad();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setTipoVialidad((String)value);
            }
        }
        ,
        NombreVialidad {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getNombreVialidad();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setNombreVialidad((String)value);
            }
        }
        ,
        NombreCorto {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getNombreCorto();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setNombreCorto((String)value);
            }
        }
        ,
        FechaAlta {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getFechaAlta();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setFechaAlta((Date)value);
            }
        }
        ,
        Estatus {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getEstatus();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setEstatus((String)value);
            }
        }
        ,
        Campo1 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo1();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo1((String)value);
            }
        }
        ,
        Campo2 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo2();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo2((String)value);
            }
        }
        ,
        Campo3 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo3();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo3((String)value);
            }
        }
        ,
        Campo4 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo4();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo4((String)value);
            }
        }
        ,
        Campo5 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo5();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo5((String)value);
            }
        }
        ,
        Campo6 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo6();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo6((String)value);
            }
        }
        ,
        Campo7 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo7();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo7((String)value);
            }
        }
        ,
        Campo8 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo8();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo8((String)value);
            }
        }
        ,
        Campo9 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo9();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo9((String)value);
            }
        }
        ,
        Campo10 {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCampo10();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setCampo10((String)value);
            }
        }
        ,
        CreadoPor {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCreadoPor();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        CreadoEl {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getCreadoEl();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ModificadoPor {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getModificadoPor();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        ModificadoEl {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getModificadoEl();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PcLocaIdentificador {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getPcLocaIdentificador();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setPcLocaIdentificador((Number)value);
            }
        }
        ,
        PrSegmentosCatastralesUbica {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getPrSegmentosCatastralesUbica();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ,
        PcLocalidadesV {
            public Object get(PcVialidadesEOImpl obj) {
                return obj.getPcLocalidadesV();
            }

            public void put(PcVialidadesEOImpl obj, Object value) {
                obj.setAttributeInternal(index(), value);
            }
        }
        ;
        private static AttributesEnum[] vals = null;
        private static int firstIndex = 0;

        public abstract Object get(PcVialidadesEOImpl object);

        public abstract void put(PcVialidadesEOImpl object, Object value);

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }

    private static PcVialidadesEODefImpl mDefinitionObject;


    public static final int IDENTIFICADOR = AttributesEnum.Identificador.index();
    public static final int CLAVEVIALIDAD = AttributesEnum.ClaveVialidad.index();
    public static final int TIPOVIALIDAD = AttributesEnum.TipoVialidad.index();
    public static final int NOMBREVIALIDAD = AttributesEnum.NombreVialidad.index();
    public static final int NOMBRECORTO = AttributesEnum.NombreCorto.index();
    public static final int FECHAALTA = AttributesEnum.FechaAlta.index();
    public static final int ESTATUS = AttributesEnum.Estatus.index();
    public static final int CAMPO1 = AttributesEnum.Campo1.index();
    public static final int CAMPO2 = AttributesEnum.Campo2.index();
    public static final int CAMPO3 = AttributesEnum.Campo3.index();
    public static final int CAMPO4 = AttributesEnum.Campo4.index();
    public static final int CAMPO5 = AttributesEnum.Campo5.index();
    public static final int CAMPO6 = AttributesEnum.Campo6.index();
    public static final int CAMPO7 = AttributesEnum.Campo7.index();
    public static final int CAMPO8 = AttributesEnum.Campo8.index();
    public static final int CAMPO9 = AttributesEnum.Campo9.index();
    public static final int CAMPO10 = AttributesEnum.Campo10.index();
    public static final int CREADOPOR = AttributesEnum.CreadoPor.index();
    public static final int CREADOEL = AttributesEnum.CreadoEl.index();
    public static final int MODIFICADOPOR = AttributesEnum.ModificadoPor.index();
    public static final int MODIFICADOEL = AttributesEnum.ModificadoEl.index();
    public static final int PCLOCAIDENTIFICADOR = AttributesEnum.PcLocaIdentificador.index();
    public static final int PRSEGMENTOSCATASTRALESUBICA = AttributesEnum.PrSegmentosCatastralesUbica.index();
    public static final int PCLOCALIDADESV = AttributesEnum.PcLocalidadesV.index();

    /**
     * This is the default constructor (do not remove).
     */
    public PcVialidadesEOImpl() {
    }


    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        if (mDefinitionObject == null) {
            mDefinitionObject = (PcVialidadesEODefImpl)EntityDefImpl.findDefObject("mx.tgc.model.entity.ubicacion.PcVialidadesEO");
        }
        return mDefinitionObject;
    }

    /**
     * Gets the attribute value for Identificador, using the alias name Identificador.
     * @return the Identificador
     */
    public Number getIdentificador() {
        return (Number)getAttributeInternal(IDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for Identificador.
     * @param value value to set the Identificador
     */
    public void setIdentificador(Number value) {
        setAttributeInternal(IDENTIFICADOR, value);
    }

    /**
     * Gets the attribute value for ClaveVialidad, using the alias name ClaveVialidad.
     * @return the ClaveVialidad
     */
    public String getClaveVialidad() {
        return (String)getAttributeInternal(CLAVEVIALIDAD);
    }

    /**
     * Sets <code>value</code> as the attribute value for ClaveVialidad.
     * @param value value to set the ClaveVialidad
     */
    public void setClaveVialidad(String value) {
        setAttributeInternal(CLAVEVIALIDAD, value);
    }

    /**
     * Gets the attribute value for TipoVialidad, using the alias name TipoVialidad.
     * @return the TipoVialidad
     */
    public String getTipoVialidad() {
        return (String)getAttributeInternal(TIPOVIALIDAD);
    }

    /**
     * Sets <code>value</code> as the attribute value for TipoVialidad.
     * @param value value to set the TipoVialidad
     */
    public void setTipoVialidad(String value) {
        setAttributeInternal(TIPOVIALIDAD, value);
    }

    /**
     * Gets the attribute value for NombreVialidad, using the alias name NombreVialidad.
     * @return the NombreVialidad
     */
    public String getNombreVialidad() {
        return (String)getAttributeInternal(NOMBREVIALIDAD);
        
    }

   
    

    /**
     * Sets <code>value</code> as the attribute value for NombreVialidad.
     * @param value value to set the NombreVialidad
     */
    public void setNombreVialidad(String value) {
        setAttributeInternal(NOMBREVIALIDAD, value);
    }

    /**
     * Gets the attribute value for NombreCorto, using the alias name NombreCorto.
     * @return the NombreCorto
     */
    public String getNombreCorto() {
        return (String)getAttributeInternal(NOMBRECORTO);
    }

    /**
     * Sets <code>value</code> as the attribute value for NombreCorto.
     * @param value value to set the NombreCorto
     */
    public void setNombreCorto(String value) {
        setAttributeInternal(NOMBRECORTO, value);
    }

    /**
     * Gets the attribute value for FechaAlta, using the alias name FechaAlta.
     * @return the FechaAlta
     */
    public Date getFechaAlta() {
        return (Date)getAttributeInternal(FECHAALTA);
    }

    /**
     * Sets <code>value</code> as the attribute value for FechaAlta.
     * @param value value to set the FechaAlta
     */
    public void setFechaAlta(Date value) {
        setAttributeInternal(FECHAALTA, value);
    }

    /**
     * Gets the attribute value for Estatus, using the alias name Estatus.
     * @return the Estatus
     */
    public String getEstatus() {
        return (String)getAttributeInternal(ESTATUS);
    }

    /**
     * Sets <code>value</code> as the attribute value for Estatus.
     * @param value value to set the Estatus
     */
    public void setEstatus(String value) {
        setAttributeInternal(ESTATUS, value);
    }

    /**
     * Gets the attribute value for Campo1, using the alias name Campo1.
     * @return the Campo1
     */
    public String getCampo1() {
        return (String)getAttributeInternal(CAMPO1);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo1.
     * @param value value to set the Campo1
     */
    public void setCampo1(String value) {
        setAttributeInternal(CAMPO1, value);
    }

    /**
     * Gets the attribute value for Campo2, using the alias name Campo2.
     * @return the Campo2
     */
    public String getCampo2() {
        return (String)getAttributeInternal(CAMPO2);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo2.
     * @param value value to set the Campo2
     */
    public void setCampo2(String value) {
        setAttributeInternal(CAMPO2, value);
    }

    /**
     * Gets the attribute value for Campo3, using the alias name Campo3.
     * @return the Campo3
     */
    public String getCampo3() {
        return (String)getAttributeInternal(CAMPO3);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo3.
     * @param value value to set the Campo3
     */
    public void setCampo3(String value) {
        setAttributeInternal(CAMPO3, value);
    }

    /**
     * Gets the attribute value for Campo4, using the alias name Campo4.
     * @return the Campo4
     */
    public String getCampo4() {
        return (String)getAttributeInternal(CAMPO4);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo4.
     * @param value value to set the Campo4
     */
    public void setCampo4(String value) {
        setAttributeInternal(CAMPO4, value);
    }

    /**
     * Gets the attribute value for Campo5, using the alias name Campo5.
     * @return the Campo5
     */
    public String getCampo5() {
        return (String)getAttributeInternal(CAMPO5);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo5.
     * @param value value to set the Campo5
     */
    public void setCampo5(String value) {
        setAttributeInternal(CAMPO5, value);
    }

    /**
     * Gets the attribute value for Campo6, using the alias name Campo6.
     * @return the Campo6
     */
    public String getCampo6() {
        return (String)getAttributeInternal(CAMPO6);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo6.
     * @param value value to set the Campo6
     */
    public void setCampo6(String value) {
        setAttributeInternal(CAMPO6, value);
    }

    /**
     * Gets the attribute value for Campo7, using the alias name Campo7.
     * @return the Campo7
     */
    public String getCampo7() {
        return (String)getAttributeInternal(CAMPO7);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo7.
     * @param value value to set the Campo7
     */
    public void setCampo7(String value) {
        setAttributeInternal(CAMPO7, value);
    }

    /**
     * Gets the attribute value for Campo8, using the alias name Campo8.
     * @return the Campo8
     */
    public String getCampo8() {
        return (String)getAttributeInternal(CAMPO8);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo8.
     * @param value value to set the Campo8
     */
    public void setCampo8(String value) {
        setAttributeInternal(CAMPO8, value);
    }

    /**
     * Gets the attribute value for Campo9, using the alias name Campo9.
     * @return the Campo9
     */
    public String getCampo9() {
        return (String)getAttributeInternal(CAMPO9);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo9.
     * @param value value to set the Campo9
     */
    public void setCampo9(String value) {
        setAttributeInternal(CAMPO9, value);
    }

    /**
     * Gets the attribute value for Campo10, using the alias name Campo10.
     * @return the Campo10
     */
    public String getCampo10() {
        return (String)getAttributeInternal(CAMPO10);
    }

    /**
     * Sets <code>value</code> as the attribute value for Campo10.
     * @param value value to set the Campo10
     */
    public void setCampo10(String value) {
        setAttributeInternal(CAMPO10, value);
    }

    /**
     * Gets the attribute value for CreadoPor, using the alias name CreadoPor.
     * @return the CreadoPor
     */
    public String getCreadoPor() {
        return (String)getAttributeInternal(CREADOPOR);
    }


    /**
     * Gets the attribute value for CreadoEl, using the alias name CreadoEl.
     * @return the CreadoEl
     */
    public Date getCreadoEl() {
        return (Date)getAttributeInternal(CREADOEL);
    }


    /**
     * Gets the attribute value for ModificadoPor, using the alias name ModificadoPor.
     * @return the ModificadoPor
     */
    public String getModificadoPor() {
        return (String)getAttributeInternal(MODIFICADOPOR);
    }


    /**
     * Gets the attribute value for ModificadoEl, using the alias name ModificadoEl.
     * @return the ModificadoEl
     */
    public Date getModificadoEl() {
        return (Date)getAttributeInternal(MODIFICADOEL);
    }


    /**
     * Gets the attribute value for PcLocaIdentificador, using the alias name PcLocaIdentificador.
     * @return the PcLocaIdentificador
     */
    public Number getPcLocaIdentificador() {
        return (Number)getAttributeInternal(PCLOCAIDENTIFICADOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for PcLocaIdentificador.
     * @param value value to set the PcLocaIdentificador
     */
    public void setPcLocaIdentificador(Number value) {
        setAttributeInternal(PCLOCAIDENTIFICADOR, value);
    }

    /**
     * getAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param attrDef the attribute

     * @return the attribute value
     * @throws Exception
     */
    protected Object getAttrInvokeAccessor(int index,
                                           AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            return AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].get(this);
        }
        return super.getAttrInvokeAccessor(index, attrDef);
    }

    /**
     * setAttrInvokeAccessor: generated method. Do not modify.
     * @param index the index identifying the attribute
     * @param value the value to assign to the attribute
     * @param attrDef the attribute

     * @throws Exception
     */
    protected void setAttrInvokeAccessor(int index, Object value,
                                         AttributeDefImpl attrDef) throws Exception {
        if ((index >= AttributesEnum.firstIndex()) && (index < AttributesEnum.count())) {
            AttributesEnum.staticValues()[index - AttributesEnum.firstIndex()].put(this, value);
            return;
        }
        super.setAttrInvokeAccessor(index, value, attrDef);
    }


    /**
     * @return the associated entity oracle.jbo.RowIterator.
     */
    public RowIterator getPrSegmentosCatastralesUbica() {
        return (RowIterator)getAttributeInternal(PRSEGMENTOSCATASTRALESUBICA);
    }

    /**
     * Uses the link PcLocaPcVialiFkLink to return rows of PcVialidadesVO
     */
    public PcLocalidadesVVORowImpl getPcLocalidadesV() {
        return (PcLocalidadesVVORowImpl)getAttributeInternal(PCLOCALIDADESV);
    }


    /**
     * @param identificador key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number identificador) {
        return new Key(new Object[]{identificador});
    }

    /**
     * Add attribute defaulting logic in this method.
     * @param attributeList list of attribute names/values to initialize the row
     */
    protected void create(AttributeList attributeList) {
      super.create(attributeList);
      SequenceImpl sq = new SequenceImpl("PC_VIAL_SEQ", this.getDBTransaction()); 
      this.setIdentificador(sq.getSequenceNumber());
    }

    /**
     * Add entity remove logic in this method.
     */
    public void remove() {
        super.remove();
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }
}
