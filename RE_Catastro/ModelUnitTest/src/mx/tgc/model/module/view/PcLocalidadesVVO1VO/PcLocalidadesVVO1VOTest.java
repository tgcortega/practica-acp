package mx.tgc.model.module.view.PcLocalidadesVVO1VO;

import mx.tgc.model.module.applicationModule.CataSegCatastralesAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PcLocalidadesVVO1VOTest {
    private CataSegCatastralesAMFixture fixture1 = CataSegCatastralesAMFixture.getInstance();

    public PcLocalidadesVVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PcLocalidadesVVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
