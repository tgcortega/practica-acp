package mx.tgc.model.module.view.PrCaracteristicasPrediosVO1VO;

import mx.tgc.model.module.applicationModule.ConsultaAMFixture;

import oracle.jbo.ViewObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrCaracteristicasPrediosVO1VOTest {
    private ConsultaAMFixture fixture1 = 
        ConsultaAMFixture.getInstance();

    public PrCaracteristicasPrediosVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrCaracteristicasPrediosVO1");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
