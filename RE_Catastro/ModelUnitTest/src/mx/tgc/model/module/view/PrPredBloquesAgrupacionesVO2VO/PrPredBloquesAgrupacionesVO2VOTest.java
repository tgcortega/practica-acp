package mx.tgc.model.module.view.PrPredBloquesAgrupacionesVO2VO;

import mx.tgc.model.module.applicationModule.ConsultaAMFixture;

import oracle.jbo.ViewObject;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class PrPredBloquesAgrupacionesVO2VOTest {
    private ConsultaAMFixture fixture1 = ConsultaAMFixture.getInstance();

    public PrPredBloquesAgrupacionesVO2VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrPredBloquesAgrupacionesVO2");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
