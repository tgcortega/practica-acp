package mx.tgc.model.module.view.PrTiposMovsPrediosUsuarios1VO;

import mx.tgc.model.module.CatalogoSencilloAMImpl;
import mx.tgc.model.module.applicationModule.CatalogoSencilloAMFixture;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class PrTiposMovsPrediosUsuarios1VOTest {
  private CatalogoSencilloAMFixture fixture1 =
      CatalogoSencilloAMFixture.getInstance();

    public PrTiposMovsPrediosUsuarios1VOTest() {
    }

  @Test
  public void testAccess() {
      ViewObject view =
          fixture1.getApplicationModule().findViewObject("PrTiposMovimientosPredios1");
      assertNotNull(view);
  }

  @Before
  public void setUp() {
  }

  @After
  public void tearDown() {
  }

  /**
   * M�todo que sirve para probar si la vista se inicializa correctamente para
   * aceptar peticiones de altas, bajas, cambios y consultas.
   *
   * @author: Eduardo Vidal
   * @since: 15-SEP-2011
   */
  @Test
  public void testCreacionInstancia() {
      ApplicationModule am = fixture1.getApplicationModule();
      ViewObject view = am.findViewObject("PrTiposMovsPrediosUsuariosVO1");
      assertNotNull(view);
  }

  /**
   * M�todo que sirve para probar los campos obligatorios
   * verifica que efectivamente si no se llenan los campos obligatorios
   * al validar el nuevo registro debe arrojar error.
   *
   * @author: Eduardo Vidal
   * @since: 15-SEP-2011
   */
  @Test
  public void testCamposObligatorios() {
      CatalogoSencilloAMImpl am =
          (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
      ViewObject view = am.findViewObject("PrTiposMovsPrediosUsuariosVO1");
      Row row = view.createRow();
      row.setAttribute("AaUsuaUsuario", "JUNIT");
      row.setAttribute("PrTmprSerie", 2011);
      row.setAttribute("PrTmprIdentificador", 1);

      row.validate();
      am.getDBTransaction().rollback();
  }


  /**
   * M�todo utilizado para probar la longitud de los campos
   * de una vista de datos, el m�todo prueba que las longitudes
   * sean probadas con la m�xima longitud permitida para los
   * campos de tipo String (VARCHAR2).
   *
   * @author: Eduardo Vidal
   * @since 15-SEP-2011
   */
  @Test
  public void testPresicionMaximaEnCampos() {
      CatalogoSencilloAMImpl am =
          (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
      ViewObject view = am.findViewObject("PrTiposMovsPrediosUsuariosVO1");
      Row row = view.createRow();

      String campo30Chars = "";
      for (int i = 0; i < 30; i++)
          campo30Chars += "X";

      row.setAttribute("AaUsuaUsuario", campo30Chars);
      row.setAttribute("PrTmprSerie", 2011);
      row.setAttribute("PrTmprIdentificador", new Long(123456789));

      row.validate();
      am.getDBTransaction().rollback();
  }

  /**
   * M�todo utilizado para probar altas y
   * bajas de registros en la vista de datos
   * se crea un nuevo row al cual se le llena con datos
   * de prueba para poder probar las operaciones de
   * insert y delete.
   *
   * @author: Eduardo Vidal
   * @since 15-SEP-2011
   */
  @Test
  public void testInsertUpdateDelete() {
      CatalogoSencilloAMImpl am =
          (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
      ViewObject view = am.findViewObject("PrTiposMovsPrediosUsuariosVO1");

      Row row = view.createRow();

      row.setAttribute("AaUsuaUsuario", "GAGUAYO");
      row.setAttribute("PrTmprSerie", 2011);
      row.setAttribute("PrTmprIdentificador", 1);

      am.getDBTransaction().commit();
      am.getDBTransaction().commit();
      row.remove();
      am.getDBTransaction().commit();

  }
}
