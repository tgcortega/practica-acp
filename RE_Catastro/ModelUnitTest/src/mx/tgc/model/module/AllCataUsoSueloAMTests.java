package mx.tgc.model.module;

import mx.tgc.model.module.applicationModule.CataUsoSueloAMFixture;
import mx.tgc.model.module.applicationModule.CataUsoSueloAMTest;
import mx.tgc.model.module.view.PrGirosVO1VO.PrGirosVO1VOTest;
import mx.tgc.model.module.view.PrGirosVO2VO.PrGirosVO2VOTest;
import mx.tgc.model.module.view.PrGirosVO3VO.PrGirosVO3VOTest;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses( { PrGirosVO3VOTest.class, PrGirosVO2VOTest.class,
                       PrGirosVO1VOTest.class, CataUsoSueloAMTest.class })
public class AllCataUsoSueloAMTests {
    @BeforeClass
    public static void setUp() {
    }

    @AfterClass
    public static void tearDown() throws Exception {
        CataUsoSueloAMFixture.getInstance().release();
    }
}
