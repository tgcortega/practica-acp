package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CatalogoSencilloAMFixture {
    private static CatalogoSencilloAMFixture fixture1 = new CatalogoSencilloAMFixture();
    private ApplicationModule _am;

    private CatalogoSencilloAMFixture() {
        _am = Configuration.createRootApplicationModule("mx.tgc.model.module.CatalogoSencilloAM","CatalogoSencilloAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CatalogoSencilloAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}
