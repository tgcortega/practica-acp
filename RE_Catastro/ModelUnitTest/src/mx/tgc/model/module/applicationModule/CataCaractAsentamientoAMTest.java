package mx.tgc.model.module.applicationModule;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataCaractAsentamientoAMImpl;

import mx.tgc.model.ro.ubicacion.PcLocalidadesVVOImpl;
import mx.tgc.model.ro.ubicacion.PcMunicipiosRecaudacionesCustomVVOImpl;

import mx.tgc.model.view.objeto.PcAsentamientosVOImpl;
import mx.tgc.model.view.ubicacion.PrCaractAsentamientosVOImpl;

import mx.tgc.model.view.ubicacion.PrCaractAsentamientosVORowImpl;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.domain.Number;

import org.junit.*;
import static org.junit.Assert.*;

public class CataCaractAsentamientoAMTest {
    private static CataCaractAsentamientoAMFixture fixture1 =
        CataCaractAsentamientoAMFixture.getInstance();
    private static CataCaractAsentamientoAMImpl _amImpl =
        (CataCaractAsentamientoAMImpl)fixture1.getApplicationModule();

    public CataCaractAsentamientoAMTest() {
    }

    /**
     * M�todo usado para inicializar todo lo necesario para las pruebas.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @BeforeClass
    public static void setUp() {
        insertFiltrarMunicipiosXRecaudacion();
    }

    /**
     * M�todo usado para borrar todo lo creado para las pruebas.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @AfterClass
    public static void tearDown() {
        deleteFiltrarMunicipiosXRecaudacion();
    }

    /**
     * M�todo usado para probar filtrarMunicipiosXRecaudacion.
     * Ejecuta el m�todo, luego se cuenta el n�mero de registros que deja el
     * filtro, de obtener diferente n�mero del esperado, devolvemos un mensaje
     * indicando cual fu� el resultado.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testFiltrarMunicipiosXRecaudacion() {
        _amImpl.filtrarMunicipiosXRecaudacion("999999999999999");
        PcMunicipiosRecaudacionesCustomVVOImpl view =
            _amImpl.getPcMunicipiosRecaudacionesCustomVVO1();
        int cantidadMunicipios = view.getRowCount();
        assertTrue("La cantidad esperada de municipios es 1 y se recibieron: " +
                   cantidadMunicipios, cantidadMunicipios == 1);
    }

    @Test
    public void testFiltrarPorRecaudacion() {
    }

     /**
      * M�todo usado para la prueba unitaria de filtrarMunicipiosXRecaudacion, 
      * y que inserta los registros necesarios para la prueba.
      *
      * @author Isaac S�as Guti�rrez
      * @since 20-SEP-2011
      */    
    private static void insertFiltrarMunicipiosXRecaudacion() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert into PC_ESTADOS (IDENTIFICADOR,CLAVE_ESTADO,DESCRIPCION,NOMBRE_CORTO,CAPITAL,TOTAL_MUNICIPIOS,ESTATUS,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) values (999999999999999,'EDO','ESTADO_PRUEBA','EDO',null,1,'AC',null,null,null,null,null)");
            stmt.execute("Insert into PC_MUNICIPIOS (IDENTIFICADOR,PC_ESTA_IDENTIFICADOR,CLAVE_MUNICIPIO,DESCRIPCION,NOMBRE_CORTO,CABECERA_MUNICIPAL,ESTATUS,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) values (999999999999999,999999999999999,'MUN','MUNICIPIO_PRUEBA','MUNI',null,'AC',null,null,null,null,null)");
            stmt.execute("Insert into CC_RECAUDACIONES (IDENTIFICADOR,CLAVE,NOMBRE,ESTATUS,DOMICILIO,RECAUDADOR,CC_RECA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,OPERACION) values (999999999999999,'CLAVE','NOMBRE','AC',null,null,null,null,null,null,null,null,'VN')");
            stmt.execute("Insert into PC_RECAUDACIONES_MUNICIPIOS (CC_RECA_IDENTIFICADOR,PC_MUNI_IDENTIFICADOR,ESTATUS,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5) values (999999999999999,999999999999999,'AC',null,null,null,null,null)");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo usado para la prueba unitaria de filtrarMunicipiosXRecaudacion, y 
     * que borra todo lo creado para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    private static void deleteFiltrarMunicipiosXRecaudacion() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("delete from pc_recaudaciones_municipios where cc_reca_identificador = 999999999999999 and pc_muni_identificador = 999999999999999");
            stmt.execute("delete from cc_recaudaciones where identificador = 999999999999999");
            stmt.execute("delete from pc_municipios where identificador = 999999999999999");
            stmt.execute("delete from pc_estados where identificador = 999999999999999");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }
}
