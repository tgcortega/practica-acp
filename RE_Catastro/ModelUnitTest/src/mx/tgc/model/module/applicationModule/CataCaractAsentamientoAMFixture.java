package mx.tgc.model.module.applicationModule;

import oracle.jbo.ApplicationModule;
import oracle.jbo.client.Configuration;

public class CataCaractAsentamientoAMFixture {
    private static CataCaractAsentamientoAMFixture fixture1 = new CataCaractAsentamientoAMFixture();
    private ApplicationModule _am;

    private CataCaractAsentamientoAMFixture() {
        _am = Configuration.createRootApplicationModule("mx.tgc.model.module.CataCaractAsentamientoAM","CataCaracAsentamientoAMLocal");
    }

    public void setUp() {
    }

    public void tearDown() {
    }

    public static CataCaractAsentamientoAMFixture getInstance() {
        return fixture1;
    }

    public void release() throws Exception {
        Configuration.releaseRootApplicationModule(_am, true);
    }

    public ApplicationModule getApplicationModule() {
        return _am;
    }
}
